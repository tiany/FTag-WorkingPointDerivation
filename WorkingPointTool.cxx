#include "WorkingPointTool.h"


// 1st constructor
WorkingPoint::WorkingPoint(TH1* hsig, TH1* hbkg){
	//m_hsig = hsig;// may not actually need this
	//m_hbkg = hbkg;
	TString type = hsig->IsA()->GetName();// Return the actual type of the object.
	if( type.Contains("TH1") ){
		init1D(hsig, hbkg);// Everything happens in init1D
	}else{
		std::cout<< "Error in WorkingPoint::WorkingPoint(): histograms should be TH1! This histogram has type: "<<type<<std::endl;
	}
}


// 2 constructor that automatically gets flat rejection
WorkingPoint::WorkingPoint(TH1 *hsig, TH1 *hbkg, double rej) {
	//std::cout<<"Using 2nd flatRej rejection tool!"<<std::endl;
	//m_hsig = hsig;
	//m_hbkg = hbkg;
	TString type = hsig->IsA()->GetName();
	if( type.Contains("TH1") ){
		std::cout<<"This constructor only takes TH2. This is a TH1."<<std::endl;
		//init1D(hsig, hbkg);
	}else if( type.Contains("TH2") ){
		//std::cout<<"Using initRej2D"<<std::endl;
		initRej2D((TH2*)hsig, (TH2*)hbkg, rej, "rej");
	}else if( type.Contains("TH3") ) {
		std::cout<<"!!! This constructor only takes TH2. The input is a TH3. Currently nowhere in the code uses this method to process a TH3. Please check."<<std::endl;
	}
	else {
		std::cout << "!!! Histogram type is not supported." << std::endl;
	}
}


// 3 uses initRej2D, initRejAndEffwithCut2D
WorkingPoint::WorkingPoint(TH1 *hsig, TH1 *hbkg, double eff, double cut, double rej, double sob){
	//m_hsig = hsig;
	//m_hbkg = hbkg;
	TString type = hsig->IsA()->GetName();
	if( type.Contains("TH1") ){// Didn't use this function
		std::cout<<"This constructor only takes TH2. This is a TH1."<<std::endl;
		//init1D(hsig, hbkg);
	}else if( type.Contains("TH2") ){
		//std::cout<<"Using initRej2D"<<std::endl;
		initRej2D((TH2*)hsig, (TH2*)hbkg, eff, "eff");
		initRejAndEffwithCut2D((TH2*)hsig, (TH2*)hbkg, cut);
		//initRejAndEffwithSoB2D((TH2*)hsig, (TH2*)hbkg, sob);
	}else if( type.Contains("TH3") ) {
		std::cout<<"!!! This constructor only takes TH2. The input is a TH3. Currently nowhere in the code uses this method to process a TH3. Please check."<<std::endl;
	}
	else {
		std::cout << "Histogram type is not supported." << std::endl;
	}
}


// 4 used in Hybrid
// to compute efficiency and rejection versus variable X (and Y) (arguments are actual TH2(/3)):
WorkingPoint::WorkingPoint(TH1 *hsig, TH1 *hbkg, TGraphErrors *effshape, double eff){
	//m_hsig = hsig;
	//m_hbkg = hbkg;
	TString type = hsig->IsA()->GetName();
	if( type.Contains("TH1") ){
		std::cout<<"This constructor only takes TH2. This is a TH1."<<std::endl;
	}else if( type.Contains("TH2") ){// TH2 although TH1 in parameters
		initRej2D((TH2*)hsig, (TH2*)hbkg, effshape, eff);
	}else{
		std::cout << "!!! Error in WorkingPoint::WorkingPoint(): This constructor only takes TH2. The input is a: "<<type<<std::endl;
	}
}



// 1.1 Creates m_rejVsEff, m_cutVsEff
void WorkingPoint::init1D(TH1 *hsig, TH1 *hbkg) {
	int nbins = hsig->GetNbinsX();
	m_rejVsEff = new TGraphErrors(nbins);
	m_cutVsEff = new TGraphErrors(nbins);
	int inf = 0;
	int sup = nbins+1;
	//std::cout<<"sup: "<<sup<<" nbins: "<<nbins<<std::endl;

	double Isig = hsig->Integral(inf,sup);//std::cout<<"Isig: "<<Isig<<std::endl;
	double Ibkg = hbkg->Integral(inf,sup);//std::cout<<"Ibkg: "<<Ibkg<<std::endl;

	int point = 0;
	for(int cut=inf; cut<sup; cut++){//std::cout<<"cut: "<<cut<<std::endl;//0-1999
		double sig = hsig->Integral(cut,sup);
		double bkg = hbkg->Integral(cut,sup);
		double cutVal = hsig->GetBinLowEdge(cut);
		double eff = 0;
		double rej = 0;
		double efferr = 0;
		double rejerr = 0;
		if(Isig != 0){
			eff = sig / Isig;
			efferr = sqrt( eff * ( 1 - eff ) / Isig );
		}
		if(bkg != 0){
			rej = Ibkg / bkg;
			rejerr = sqrt( rej * ( rej - 1 ) / bkg );
		}
		m_rejVsEff->SetPoint(point, eff, rej);
		m_rejVsEff->SetPointError(point, efferr, rejerr);
		m_cutVsEff->SetPoint(point, eff, cutVal);
		point++;
	}
	//m_cutVsEff->Print("all");
}



// 2.1 3.1 integrated rejection (for efficiency plot):
void WorkingPoint::initRej2D(TH2 *hsig, TH2 *hbkg, double effOrRej, TString option){
	//std::cout<<"in the initRej2D I wrote :o!"<<std::endl;
	int nbinsx = hsig->GetNbinsX();
	int nbinsy = hsig->GetNbinsY();
	//if (option=="eff"){
	int nCutVsPtBins=0;
	m_rejVsX = new TGraphErrors(nbinsx);
	m_cutVsX = new TGraphErrors(nbinsx);
	//}else if(option=="rej"){
	int nCutVsPtForFlatRejBins=0;
	m_cutVsX_flatRej = new TGraphErrors(nbinsx);
	m_effVsX_flatRej = new TGraphErrors(nbinsx);
	//}
	//std::cout<<"nbinsx:	"<<nbinsx<<"	nbinsy: "<<nbinsy<<std::endl;//35, 1999. pt, tw
	for(int binx=1; binx<=nbinsx; binx++) {
		double Isig = hsig->Integral(binx, binx, 0, nbinsy+1);
		double Ibkg = hbkg->Integral(binx, binx, 0, nbinsy+1);
		for(int biny=1; biny<nbinsy; biny++) {
			double siginf = hsig->Integral(binx, binx, biny, nbinsy+1);
			double bkginf = hbkg->Integral(binx, binx, biny, nbinsy+1);
			double sigsup = hsig->Integral(binx, binx, biny+1, nbinsy+1);
			double sigsupNext = hsig->Integral(binx, binx, biny+2, nbinsy+1);
			double bkgsup = hbkg->Integral(binx, binx, biny+1, nbinsy+1);

			double effinf = 0;   double effsup = 0;    double efferr = 0;
			double effInter = 0; double effInterErr=0; double efferrNext=0;
			double rejinf = 0;   double rejsup = 0;    double rejerr = 0;
			double rejInter = 0;

			double cutInter = 0;    double cuterr = 0;
			double winf = hsig->GetYaxis()->GetBinCenter(biny);
			double wsup = hsig->GetYaxis()->GetBinCenter(biny+1); 
			//signal or not, it should hold the same bin nb

			//////////////////////////////////
			if(Isig != 0) {
				effinf = siginf / Isig;
				effsup = sigsup / Isig;
				efferr = sqrt( sigsup*(Isig-sigsup)/Isig )/Isig;
				efferrNext = sqrt( sigsupNext*(Isig-sigsupNext)/Isig )/Isig;
			}
			if(bkginf != 0 && bkgsup != 0) {
				rejinf = Ibkg / bkginf;
				rejsup = Ibkg / bkgsup;
				rejerr = sqrt( rejinf * ( rejinf - 1 ) / bkginf );
			}
			//std::cout<<"binx: "<<binx<<" biny: "<<biny<<" rejinf: "<<rejinf<<" rejsup: "<<rejsup<<" effinf, effsup: "<<effinf<<" "<<effsup<<" siginf, bkginf: "<<siginf<<" "<<bkginf<<" Isig, Ibkg: "<<Isig<<" "<<Ibkg<<std::endl;
			bool fitsRequirement=false;
			if (option=="eff"){
				if(effinf >= effOrRej && effsup <= effOrRej){// && flatEffPntNoted==false){
					rejInter = rejinf;
					if(effinf != effsup){// Otherwise divide by 0
					rejInter = rejinf + (rejinf - rejsup)*(effOrRej - effinf)/(effinf - effsup);
					}//else{
						//std::cout<<"inf==sup!"<<std::endl;
					//}
					m_rejVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), rejInter);
					m_rejVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, rejerr);
	
					// cut Vs pT
					cutInter = winf;
					if(effinf != effsup){
					cutInter=winf +(winf - wsup)*(effOrRej - effinf)/(effinf - effsup);
					}
					m_cutVsX->SetPoint(nCutVsPtBins, hsig->GetXaxis()->GetBinCenter(binx), cutInter);
					//std::cout<<"fEff! "<<nCutVsPtBins<<" cut: "<<cut<<"	binx-1: "<<binx-1<<"	pt: "<<hsig->GetXaxis()->GetBinCenter(binx)<<std::endl;//Yusong: originally it's binx-1 instead of nCutVsPtBins. This two would be different if there is one skipped point)
					m_cutVsX->SetPointError(nCutVsPtBins, hsig->GetXaxis()->GetBinWidth(nCutVsPtBins)/2, hsig->GetYaxis()->GetBinWidth(0)/2);
					nCutVsPtBins=nCutVsPtBins+1;
					// hsig->GetXaxis() outputs 0x5dbae48, probably just getting the axis to do things to it, e.g. GetBinCenter on a certain bin.
					//std::cout<<binx<<" "<<biny<<" eff: "<<effinf<<" "<<effsup<<" cut: "<<cut<< " "<<cutInter<<" "<<wsup<<" rej: "<<rejinf<<" "<<rejsup<<std::endl;
					break;
				}
			}else if(option=="rej"){// get cut vs pt for flat rej.
				// check if rejinf and sup are the same when effinf==infsup.//No.
				//if (effinf==effsup){
				//	std::cout<<binx<<" "<<biny<<" eff: "<<effinf<<" "<<effsup<<" rej: "<<rejinf<<" "<<rejsup<<std::endl;
				//}
				//rejinf increase.
				if(rejinf <=effOrRej && rejsup >= effOrRej){// && flatRejPntNoted==false) {//Yusong: several bin with same rejinf. So this will... underestimate the tw?
					cutInter = winf;
					effInter = effinf;
					effInterErr = efferr;
					if(rejinf != rejsup){
						cutInter = winf+ (winf-wsup)*(effOrRej-rejinf)/(rejinf-rejsup);
						effInter = effinf + (effsup-effinf)*(effOrRej-rejinf)/(rejsup-rejinf);
						effInterErr= efferr + (efferrNext - efferr)*(effOrRej-rejinf)/(rejsup - rejinf);
					}//else{//std::cout<<"somehow rejinf == rejsup!"<<std::endl;}
					m_cutVsX_flatRej->SetPoint(nCutVsPtForFlatRejBins,hsig->GetXaxis()->GetBinCenter(binx),cutInter);
					m_cutVsX_flatRej->SetPointError(nCutVsPtForFlatRejBins, hsig->GetXaxis()->GetBinWidth(nCutVsPtForFlatRejBins)/2, hsig->GetYaxis()->GetBinWidth(0)/2);
					//m_cutVsX_flatRej->SetPointError(nCutVsPtBins, hsig->GetXaxis()->GetBinWidth(nCutVsPtBins)/2, hsig->GetYaxis()->GetBinWidth(0)/2);//used the wrong bin numbering? 2020-10-4. Check result difference?
					m_effVsX_flatRej->SetPoint(nCutVsPtForFlatRejBins,hsig->GetXaxis()->GetBinCenter(binx),effInter);
					m_effVsX_flatRej->SetPointError(nCutVsPtForFlatRejBins, hsig->GetXaxis()->GetBinWidth(nCutVsPtForFlatRejBins)/2, effInterErr);//GetXaxis()->GetBinWidth(binx) should be???
					nCutVsPtForFlatRejBins=nCutVsPtForFlatRejBins+1;
					break;
				}
			}
		}
	}

	if (option=="eff"){
		//Yusong: get rid of the extra points in m_cutVsX
		for (int i=nbinsx-1;i>=nCutVsPtBins;i--){
			//std::cout<<"i: "<<i<<std::endl;
			m_cutVsX->RemovePoint(i);
		}
	}else if(option=="rej"){
		for (int i=nbinsx-1;i>=nCutVsPtForFlatRejBins;i--){
			//std::cout<<"i: "<<i<<std::endl;
			m_cutVsX_flatRej->RemovePoint(i);
			m_effVsX_flatRej->RemovePoint(i);
		}
	}
}



// 4.1
void WorkingPoint::initRej2D(TH2 *hsig, TH2 *hbkg, TGraphErrors *effshape, double localeff) {
	int nbinsx = hsig->GetNbinsX();
	int nbinsy = hsig->GetNbinsY();
	int nCutVsPtBins=0;
	//std::cout<<"nbinsx: "<<nbinsx<<" nbinsy: "<<nbinsy<<std::endl;//35, 1999. pt, tw
	m_rejVsX = new TGraphErrors(nbinsx);
	m_cutVsX = new TGraphErrors(nbinsx);
	double lighteff = 0.;
	double Nlighttagged = 0.;
	double Itot=0;
	double Iweighteff=0;
	double Itot_bkg=0;
	double Iweightmis_bkg=0;
	for(int binx=1; binx<=nbinsx; binx++) {// binx=0 weird. Underflow bin
		//# ------ flatEfficiency ------Writing FlatBEff_70 cutValues in custom CDI. Tagger: DL1r
		//cutvsptn: 35
		//0 -25.5952 
		//1 11.25 
		//2 13.75 
		//3 16.25 
		double Isig = hsig->Integral(binx, binx, 0, nbinsy+1);
		double Ibkg = hbkg->Integral(binx, binx, 0, nbinsy+1);
		double eff = effshape->GetY()[binx-1];
		//std::cout<<eff<<" - Integral: "<<Isig<<std::endl;
		Itot=Itot+Isig;
		Iweighteff=Iweighteff+Isig*eff;

		//Itot_bkg=Itot_bkg+Isig_bkg;
		//Iweighteff_bkg=Iweighteff_bkg+Isig_bkg*eff_bkg;

		//double eff = 0;
		//double Xeff = 0;
		//effshape->GetPoint(binx-1,Xeff,eff);
	for(int biny=1; biny<nbinsy; biny++) {
		double siginf = hsig->Integral(binx, binx, biny, nbinsy+1);
		double bkginf = hbkg->Integral(binx, binx, biny, nbinsy+1);
		double sigsup = hsig->Integral(binx, binx, biny+1, nbinsy+1);
		double bkgsup = hbkg->Integral(binx, binx, biny+1, nbinsy+1);
		double effinf = 0; double effsup = 0; double efferr = 0;
		double rejinf = 0; double rejsup = 0; double rej = 0;
		double rejerr = 0;
		//Experimental stuff for pT-dependent benchmarks

		double cut = 999; double cuterr = 0;
		double winf = hsig->GetYaxis()->GetBinCenter(biny);
		double wsup = hsig->GetYaxis()->GetBinCenter(biny+1);

		//////////////////////////////////
		if(Isig != 0) {
			effinf = siginf / Isig;
			effsup = sigsup / Isig;
			efferr = sqrt( sigsup*(Isig-sigsup)/Isig )/Isig;
		}
		if(bkginf != 0 && bkgsup != 0) {
	    	rejinf = Ibkg / bkginf;
	    	rejsup = Ibkg / bkgsup;
	    	rejerr = sqrt( rejinf * ( rejinf - 1 ) / bkginf );
	    	cuterr = sqrt( rejinf * ( rejinf - 1 ) / bkginf );
		}
      // fill rejection:

      if(effinf >= eff && effsup <= eff) {
	      rej = rejinf;
	      if(effinf != effsup) rej = rejinf +
			  	   (rejinf - rejsup)*(eff - effinf)/(effinf - effsup);
	      m_rejVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), rej);
	      m_rejVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, rejerr);

	      lighteff = 1./rej;
	      Nlighttagged += lighteff*Ibkg;

	      // cut Vs pT
	      cut = winf;
	      if(effinf != effsup) cut = winf +
			  	   (winf - wsup)*(eff - effinf)/(effinf - effsup);
			m_cutVsX->SetPoint(nCutVsPtBins, hsig->GetXaxis()->GetBinCenter(binx), cut);
			m_cutVsX->SetPointError(nCutVsPtBins, hsig->GetXaxis()->GetBinWidth(nCutVsPtBins)/2, hsig->GetYaxis()->GetBinWidth(0)/2);
			//m_cutVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), cut);
			//std::cout<<nCutVsPtBins<<"    cut:    "<<cut<<"   binx-1:    "<<binx-1<<" pt:    "<<hsig->GetXaxis()->GetBinCenter(binx)<<std::endl;
			nCutVsPtBins=nCutVsPtBins+1;
			break;
			}
		}
	}
	for (int i=nbinsx-1;i>=nCutVsPtBins;i--){
		//std::cout<<"i: "<<i<<std::endl;
		m_cutVsX->RemovePoint(i);
	}
}



// 1.2 not directly used by constructor 1, but used by instances created by 1
// Uses getRejWithErr
void WorkingPoint::getRej(double eff, double &r, double &re, double &w, double &we) {
  double *rej = this->getRejWithErr(eff);
  r = rej[0];
  re = rej[1];
  w = rej[2];
  we=rej[3];
  //std::cout<<"r e w: "<<r<<"	"<<e<<"	"<<w<<std::endl;
  delete [] rej;
}
// gets the rejection with error, when the bin has wanted efficiency.
double *WorkingPoint::getRejWithErr(double eff) {
	double *rej = new double[4];
	int np = m_rejVsEff->GetN();
	for(unsigned int p=0; p<np-1; p++){// <np-1 are we losing a point here?//Yes and should. because using p+1 later on.
		double effinf, rejinf, rejerrinf, cutinf;
		double effsup, rejsup, rejerrsup, cutsup;
		m_cutVsEff->GetPoint(p, effinf, cutinf);
		m_cutVsEff->GetPoint(p+1, effsup, cutsup);
		//std::cout<<"effinf	cutinf:	"<<effinf<<"    "<<cutinf<<std::endl;
		m_rejVsEff->GetPoint(p, effinf, rejinf);
		m_rejVsEff->GetPoint(p+1, effsup, rejsup);
		//std::cout<<"effinf  rejinf: "<<effinf<<"    "<<rejinf<<std::endl;
		rejerrinf = m_rejVsEff->GetErrorY(p);
		rejerrsup = m_rejVsEff->GetErrorY(p+1);
		//if(effinf <= eff && effsup > eff){
		if(effinf >= eff && effsup <= eff){
			rej[0] = rejinf + (eff - effinf)*(rejinf-rejsup)/(effinf - effsup);
			rej[1] = rejerrinf + (eff - effinf)*(rejerrinf-rejerrsup)/(effinf - effsup);
			rej[2] = cutinf + (eff - effinf)*(cutinf-cutsup)/(effinf - effsup);
			rej[3] = (cutinf-cutsup)/2;
			break;
		}
	}
	return rej;
}



// 3.2
// gets rej vs pT and eff vs pT from a cut value
void WorkingPoint::initRejAndEffwithCut2D(TH2* hsig, TH2* hbkg, double wcut) {
	//std::cout<<"in initRejAndEffwithCut2D!"<<std::endl;
	int nbinsx = hsig->GetNbinsX();
	int nbinsy = hsig->GetNbinsY();
	m_rejFromCutVsX = new TGraphErrors(nbinsx);
	m_effFromCutVsX = new TGraphErrors(nbinsx);
	for(unsigned int binx=1; binx<=nbinsx; binx++) {
		double Isig = hsig->Integral(binx, binx, 0, nbinsy+1);
		double Ibkg = hbkg->Integral(binx, binx, 0, nbinsy+1);
		for(int biny=1; biny<nbinsy; biny++) {
			double siginf = hsig->Integral(binx, binx, biny, nbinsy+1);
			double bkginf = hbkg->Integral(binx, binx, biny, nbinsy+1);
			double sigsup = hsig->Integral(binx, binx, biny+1, nbinsy+1);
			double bkgsup = hbkg->Integral(binx, binx, biny+1, nbinsy+1);
			double effinf = 0; double effsup = 0; double eff = 0.;
			double rejinf = 0; double rejsup = 0; double rej = 0;
			double efferr = 0;
			double rejerr = 0;
			double winf = hsig->GetYaxis()->GetBinCenter(biny);// signal or not,
			double wsup = hsig->GetYaxis()->GetBinCenter(biny+1);// it should hold the same bin nb
			double werr = 0;

			if(Isig != 0) {
				effinf = siginf / Isig;
				effsup = sigsup / Isig;
				efferr = sqrt( sigsup*(Isig-sigsup)/Isig )/Isig;
			}
			if(bkginf != 0 && bkgsup != 0) {
				rejinf = Ibkg / bkginf;
				rejsup = Ibkg / bkgsup;
				rejerr = sqrt( rejinf * ( rejinf - 1 ) / bkginf );
			}
			// fill rejection:
			if((winf <= wcut && wsup >= wcut) || winf==wcut) {
				rej = rejinf;
				eff = effinf;
				if(winf != wsup){// Otherwise divide by 0
					rej = rejinf +(rejinf - rejsup)*(wcut - winf)/(winf - wsup);
					eff = effinf +(effinf - effsup)*(wcut - winf)/(winf - wsup);
				}
	m_rejFromCutVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), rej);
	m_rejFromCutVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, rejerr);

	m_effFromCutVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), eff);
	m_effFromCutVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, efferr);
				break;
			}
		}
	}
}



/*
// 3.3
void WorkingPoint::initRejAndEffwithSoB2D(TH2 *hsig, TH2 *hbkg, double SoB) {
	//std::cout<<"in initRejAndEffwithSoB2D!"<<std::endl;
  int nbinsx = hsig->GetNbinsX();
  int nbinsy = hsig->GetNbinsY();
  m_rejFromSoBVsX = new TGraphErrors(nbinsx);
  m_effFromSoBVsX = new TGraphErrors(nbinsx);

  TH2F *hlog = (TH2F*)hsig->Clone("LLR");
  hlog->Reset();

  TH2F *hratio = (TH2F*)hsig->Clone("ratio");
  hratio->Scale(1./hsig->Integral());
  TH2F *hbkgcopy = (TH2F*)hbkg->Clone("bkgcopy");
  hbkgcopy->Scale(1./hbkgcopy->Integral());
  hratio->Divide(hbkgcopy);

  for(int binx=1; binx<=nbinsx; binx++) {//x is the tagweight
    double Isig = hsig->Integral(binx, binx, 0, nbinsy+1);
    double Ibkg = hbkg->Integral(binx, binx, 0, nbinsy+1);
    for(int biny=1; biny<nbinsy; biny++) {
      if (hratio->GetBinContent(binx,biny)>0){
  	double ratinf = log(hratio->GetBinContent(binx,biny));
	double ratsup = log(hratio->GetBinContent(binx,biny+1));

	double siginf = hsig->Integral(binx, binx, biny, nbinsy+1);
	double bkginf = hbkg->Integral(binx, binx, biny, nbinsy+1);
	double sigsup = hsig->Integral(binx, binx, biny+1, nbinsy+1);
	double bkgsup = hbkg->Integral(binx, binx, biny+1, nbinsy+1);
	double eff    = 0;
	double effinf = 0;
	double rejinf = 0;
	double effsup = 0;
	double rejsup = 0;
	double rej    = 0;
	double efferr = 0;
	double rejerr = 0;
	double werr = 0;

	if(Isig != 0) {
	  effinf = siginf / Isig;
	  effsup = sigsup / Isig;
	  efferr = sqrt( sigsup*(Isig-sigsup)/Isig )/Isig;
	}
	if(bkginf != 0 && bkgsup != 0) {
	  rejinf = Ibkg / bkginf;
	  rejsup = Ibkg / bkgsup;
	  rejerr = sqrt( rejinf * ( rejinf - 1 ) / bkginf );
	}
	// fill :
	if((ratinf <= SoB && ratsup >= SoB) || ratinf==SoB) {
	  rej = rejinf;
	  eff = effinf;

	  if(ratinf != ratsup){
	    rej = rejinf +
	      (rejinf - rejsup)*(SoB - ratinf)/(ratinf - ratsup);
	    eff = effinf +
	      (effinf - effsup)*(SoB - ratinf)/(ratinf - ratsup);
	  }

	  m_rejFromSoBVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), rej);
	  m_rejFromSoBVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, rejerr);

	  m_effFromSoBVsX->SetPoint(binx-1, hsig->GetXaxis()->GetBinCenter(binx), eff);
	  m_effFromSoBVsX->SetPointError(binx-1, hsig->GetXaxis()->GetBinWidth(binx)/2, efferr);
	  break;
	}
      }
    }
  }
}
*/



//cloneRejVsEff only clones and assigns name to m_rejVsEff defined and given value previously in WorkingPoint
TGraphErrors* WorkingPoint::cloneRejVsEff(TString name){
	TGraphErrors* tmp = (TGraphErrors*)m_rejVsEff->Clone();
	tmp->SetName(name);
	return tmp;
}

TGraphErrors* WorkingPoint::getRejVsX(){
  return (TGraphErrors*)m_rejVsX->Clone();
}

TGraphErrors* WorkingPoint::getCutVsX(){
  return (TGraphErrors*)m_cutVsX->Clone();
}

TGraphErrors* WorkingPoint::getEffVsX(){// Not used...
  return (TGraphErrors*)m_effVsX->Clone();
}


TGraphErrors* WorkingPoint::getRejFromCutVsX(){
  return (TGraphErrors*)m_rejFromCutVsX->Clone();
}

TGraphErrors* WorkingPoint::getEffFromCutVsX(){
  return (TGraphErrors*)m_effFromCutVsX->Clone();
}

/*
TGraphErrors *WorkingPoint::getRejFromSoBVsX() {// not used
  return (TGraphErrors*)m_rejFromSoBVsX->Clone();
}

TGraphErrors *WorkingPoint::getEffFromSoBVsX() {// not used
  return (TGraphErrors*)m_effFromSoBVsX->Clone();//
}
*/



TGraph2DErrors* WorkingPoint::getCutVsX_flatRej(){// flat rej
  return (TGraph2DErrors*)m_cutVsX_flatRej->Clone();
}

TGraphErrors* WorkingPoint::getEffVsX_flatRej(TString name){// flat rej
  TGraphErrors* tmp = (TGraphErrors*)m_effVsX_flatRej->Clone();
  tmp->SetName(name);
  return tmp;
}


TGraph* WorkingPoint::cloneCutVsEff(TString name) {// Not used...
  TGraph* tmp = (TGraph*)m_cutVsEff->Clone();// m_... used in 1.1, 3.1, 4.1
  tmp->SetName(name);
  return tmp;
}



