// This file is really -*- C++ -*-.
#ifndef WORKINGPOINTTOOL_H
#define WORKINGPOINTTOOL_H


class WorkingPoint{

public:
  WorkingPoint(TH1 *hsig, TH1 *hbkg);// 1st constructor. Uses the method that  gets rej vs eff, cut vs eff
  WorkingPoint(TH1 *hsig, TH1 *hbkg, double rej);// 2 flat rejection
  WorkingPoint(TH1 *hsig, TH1 *hbkg, double eff, double cut, double rej, double sob=0.);// 3
  WorkingPoint(TH1 *hsig, TH1 *hbkg, TGraphErrors *effshape, double eff=0.);// 4 hybrid

  void init1D(TH1 *hsig, TH1 *hbkg);// 1.1
  void initRej2D(TH2 *hsig, TH2 *hbkg, double eff, TString option);// 2.1 (flat rej), 3.1
  void initRej2D(TH2 *hsig, TH2 *hbkg, TGraphErrors *effshape, double eff);// 4.1 (hybrid)
  void getRej(double eff, double &r, double &rerr, double &w, double &werr);// can be used on an instance constructed by constructor 1. Uses getRejWithErr
  double* getRejWithErr(double eff);// used by getRej

  void initRejAndEffwithCut2D(TH2 *hsig, TH2 *hbkg, double wcut);	// 3.2
  //void initRejAndEffwithSoB2D(TH2 *hsig, TH2 *hbkg, double SoB);	// 3.3

  TGraphErrors* cloneRejVsEff(TString name);// Used for overall rej vs eff plot
  TGraphErrors* getRejVsX();
  TGraphErrors* getCutVsX();
  TGraphErrors* getEffVsX();// Not used...
  TGraphErrors* getRejFromCutVsX();
  TGraphErrors* getEffFromCutVsX();
  //TGraphErrors* getRejFromSoBVsX();// not used
  //TGraphErrors* getEffFromSoBVsX();// not used
  TGraph2DErrors* getCutVsX_flatRej();// flat rej
  TGraphErrors*   getEffVsX_flatRej(TString name);// flat rej
  TGraph*         cloneCutVsEff(TString name);// Not used...


private:

  TGraphErrors* m_rejVsEff;
  TGraph* m_cutVsEff;
  TGraphErrors* m_rejVsX;
  TGraphErrors* m_cutVsX;
  TGraphErrors* m_effVsX;
  TGraphErrors* m_rejFromCutVsX;
  TGraphErrors* m_effFromCutVsX;
  //TGraphErrors* m_rejFromSoBVsX;
  //TGraphErrors* m_effFromSoBVsX;
  TGraphErrors* m_cutVsX_flatRej;// flat rej
  TGraphErrors* m_effVsX_flatRej;// flat rej
  //TH1* m_hsig;
  //TH1* m_hbkg;
};

#endif // WORKINGPOINTTOOL_H
