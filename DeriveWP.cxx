#include <iostream>
#include <TH3F.h>
#include <TH2F.h>
#include "TDirectory.h"
#include "TFile.h"
#include "WorkingPointTool.h"
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TString.h>
#include <TCanvas.h>
#include <TVector.h>
#include <fstream>
// fstream is for output .txt files.


// A short function to judge whether user wants a certain profile.
// Variables: a string with all profiles wanted, lenth of the string, the profile you want to check. Output true or false.
bool wantProfileOrNot(std::string WP_profiles, int len_WP_profiles, TString profileName, bool printProfile=true){
	if (WP_profiles.find(profileName)<len_WP_profiles){
		if (printProfile){std::cout<<profileName+": yes\n";}
		return true;
	}else{
		if (printProfile){std::cout<<profileName+": no\n";}
		return false;
	}
}


// A function to easily store texts or TGraphErrors in a .txt file
void saveInTxt(TString outTxtName,TGraphErrors* theProfile,TString xLabel,TString yLabel){
	std::ofstream txtFile;
	Double_t* xValues=theProfile->GetX();
	Double_t* yValues=theProfile->GetY();
	int xValues_len = theProfile->GetN();
	txtFile.open(outTxtName, std::ofstream::app);
	txtFile<<"#"<<" "<<xLabel<<"   "<<yLabel<<"\n";
	for (int i=0;i<xValues_len;i++){
		//std::string yValuesString=to_string(i)+"	"+to_string(xValues[i])+"   "+to_string(yValues[i]);// Don't do it this way. pT also keeps 6 digits, too messy.
		txtFile<<i<<" "<<xValues[i]<<"   "<<yValues[i]<<"\n";
		//cout<<setprecision(10)<<fixed<<xValues[i]<<' '<<yValues[i]<<"\n"; // Could modify to keep more digits only in y.
	}
	txtFile<<"\n\n";
	txtFile.close();
	return;
}
void saveInTxt(TString outTxtName, std::vector<TString> theText, std::vector<double> theDouble, bool printOut=true, TString fileOption="update", TString printInstead=""){
	std::ofstream txtFile;
	if (fileOption=="recreate"){
		txtFile.open(outTxtName);
	}else{
		txtFile.open(outTxtName, std::ofstream::app);
	}
	if (printOut==true){
		for (int i=0;i<theText.size() or i<theDouble.size();i++){
			if (i<theText.size() and i<theDouble.size()){
				txtFile<<theText[i]<<theDouble[i];
				std::cout<<theText[i]<<theDouble[i];
			}else if (i<theText.size() and i>=theDouble.size()){
				txtFile<<theText[i];
				std::cout<<theText[i];
			}else if (i>=theText.size() and i<theDouble.size()){
				txtFile<<theDouble[i];
				std::cout<<theDouble[i];
			}
		}
	}else{
		for (int i=0;i<theText.size() or i<theDouble.size();i++){
			if (i<theText.size() and i<theDouble.size()){
				txtFile<<theText[i]<<theDouble[i];
			}else if (i<theText.size() and i>=theDouble.size()){
				txtFile<<theText[i];
			}else if (i>=theText.size() and i<theDouble.size()){
				txtFile<<theDouble[i];
			}
		}
		if (printInstead!=""){
			std::cout<<printInstead;
		}
	}
	txtFile.close();
	return;
}


// E.g. for flat cut, writes the cut value in the customCDI
// fc are also stored as TVectors
void writeTVector(TDirectory* opdir, double cutValue, TString cutValueName){
	TVector* cutval = new TVector(1);
	cutval[0]=(cutValue) ;
	//cutval->Print();//Output: 
	//Vector (1)  is as follows
	//
	//     |        1  |
	//     ------------------
	//        0 |0.769435 
	opdir->WriteTObject( cutval, cutValueName);

	delete cutval;
	return;
}


// Writes the cut values in customCDI.
// Writes TGraph as a TSpline3.
void writeTSpline3(TDirectory* opdir, TGraph* cutVspt, TString objectName="cutvalues"){
	//cutVspt has the same x and y values, with ex and ey both 0. Despite being TGraph instead of TGraphErrors
	//cutVspt->Print("all");//(*cutline).Print();
	TSpline3 *cutline = new TSpline3("Cut Profile for Flat Efficiency", cutVspt);
	opdir->WriteTObject( cutline, objectName );
	delete cutline;
	return;
}


// Build efficiency profile for hybrid 
TGraph* hybridProfileBuilder(TGraph* g, double flat_efficiency){
	int npoints = g->GetN();// The number of points
	double fcut = flat_efficiency;// fcut will be changed.
	double *X = g->GetX();
	double *Y = g->GetY();

	double diff = 0;
	double flipped = 0;
	for (int i=0; i<npoints; i++){
		double newdiff = fcut-Y[i];
		if (diff*newdiff<0. && 0!=i){// 0!=i because diff=0 for i=0. Although shouldn't make a difference?// Possible improvement: if start from >fcut, will never reach the 2nd intersection point?
			flipped++;//std::cout <<"first if    diff: "<<diff<<" newdiff: "<<newdiff<<" "<<X[i] <<" flipped: "<<flipped<<"\n";
		}
		diff=newdiff;

		if (flipped>1){
			Y[i] = fcut;
		}
	}

	TGraph *newprof = new TGraph(npoints,X,Y);
	newprof->SetTitle("Efficiency profile for hybrid WP");
	newprof->GetXaxis()->SetTitle("pT");
	newprof->GetYaxis()->SetTitle("b-efficiency");
	newprof->SetName("effvspthybridprofile");

	return newprof;// returns efficiency profile for hybrid WP
	delete newprof;
}


// Save the cut TSpline in customCDI
void writeHybridWPFile(TDirectory* opdir, TGraph *byFDB, double minPt=-1){
	int npoints = byFDB->GetN();
	double* X = byFDB->GetX();
	double* Y = byFDB->GetY();
	if(minPt>0){// This is to "flatten" all cuts under a given pt, in order to avoid a wobble in the spline going from 0 to some cut value
		int firstPointOverPtMin = -1;
		double firstPointValue = -1;
		for (int i=0; i<npoints; i++){// Take a note for the 1st point that's larger than minPt
			if(X[i] > minPt){
				firstPointOverPtMin = i;
				firstPointValue = Y[i];
				break;
			}
		}

		for (int i=0; i<npoints; i++){// Change every point before it to that value.
			Y[i] = firstPointValue;
			if(i>firstPointOverPtMin-2){// Eg if firstP...Min=4, this i will go to 3 and then stop.
				break;
			}
		}
	}

	TGraph *newcut = new TGraph(npoints, X, Y);// "newcut" stores the hybrid profile.
	TSpline3 *cutline = new TSpline3("Cut Profile for Hybrid", newcut);
	opdir->WriteTObject(cutline, "cutvalues");
	return;
	delete newcut;
	delete cutline;
}


// Check that the cut does give the WP.
std::string check1D(double cut, TH1 *hsig, TH1 *hbkg){
	std::string returnStr="";
	int nbins = hsig->GetNbinsX();
	int inf = 0;
	int sup = nbins+1;
	double Isig = hsig->Integral(inf,sup);
	double Ibkg = hbkg->Integral(inf,sup);
	for(int p=0; p<nbins-1; p++){
		double cutinf = hsig->GetBinLowEdge(p);
		double cutsup = hsig->GetBinLowEdge(p+1);
		if (cutinf <= cut && cutsup > cut){
			double siginf = hsig->Integral(p,sup);
			double sigsup = hsig->Integral(p+1,sup);
			double effinf = 0; double effsup = 0; double eff = 0;
			double efferrinf = 0; double efferrsup = 0; double efferr = 0;
			if(Isig != 0){
				effinf = siginf / Isig;
				effsup = sigsup / Isig;
				efferrinf = sqrt( effinf * ( 1 - effinf ) / Isig );
				efferrsup = sqrt( effsup * ( 1 - effsup ) / Isig );
				eff = effinf+(effsup-effinf)*(cut-cutinf)/(cutsup-cutinf);
				efferr = efferrinf+(efferrsup-efferrinf)*(cut-cutinf)/(cutsup-cutinf);
				//std::cout<<eff<<" +- "<<efferr<<"\n";
				returnStr = std::to_string(eff)+" +- "+std::to_string(efferr);
			}else{std::cout<<"There are no entries in the histogram!\n";}
		}
	}
	return returnStr;
}


// Check the pT dependent WPs give the flat whatever back.
TGraphErrors* check2D_Spline(TGraphErrors* cutVspt, TH2* hsig, TH2* hbkg, TString graphName, TString checkOpt="eff",bool reversecut=false) {
	int nbinsx = hsig->GetNbinsX();
	int nbinsy = hsig->GetNbinsY();
	//std::cout<<"nbinsx: "<<nbinsx<<" nbinsy: "<<nbinsy<<"\n";//35, 1999. pt, tw 42
	int cutVsptN=cutVspt->GetN();
	int cutVsptNit=0;
	int binx=0;

	TGraphErrors* effVspt = new TGraphErrors(cutVsptN);
	int nEffVspt=0;
	TGraphErrors* rejVspt = new TGraphErrors(cutVsptN);
	int nRejVspt=0;
	if(checkOpt.Contains("eff")){
		effVspt->SetName(graphName);
	}else if (checkOpt.Contains("rej")){
		rejVspt->SetName(graphName);
	}
	TSpline3* cutline = new TSpline3("Cut Profile for Flat Efficiency", cutVspt);
	for(; binx<nbinsx; binx++) {
		double pTBinCentre=hsig->GetXaxis()->GetBinCenter(binx);
		double Isig = hsig->Integral(binx, binx, 0, nbinsy+1);
		double Ibkg = hbkg->Integral(binx, binx, 0, nbinsy+1);
		double cut=cutline->Eval(pTBinCentre);
		for(int biny=0; biny<nbinsy; biny++) {
			double cutInf=hsig->GetYaxis()->GetBinLowEdge(biny);
			double cutSup=hsig->GetYaxis()->GetBinUpEdge(biny);
			if(cutInf<=cut and cutSup>=cut){ 
				if(checkOpt.Contains("eff")){
					double sigInf = hsig->Integral(binx, binx, biny, nbinsy+1);
					double sigSup = hsig->Integral(binx, binx, biny+1, nbinsy+1);
					double sigSupNext = hsig->Integral(binx, binx, biny+2, nbinsy+1);
					double effInf=0; double effSup=0; double effErr=0; double effErrNext=0;
					double effInter = 0; double effErrInter=0;
					if(Isig != 0) {
						effInf = sigInf / Isig;
						effSup = sigSup / Isig;
						effErr = sqrt( sigSup*(Isig-sigSup)/Isig )/Isig;
						effErrNext = sqrt( sigSupNext*(Isig-sigSupNext)/Isig )/Isig;
						effInter = effInf + (effSup-effInf)*(cut-cutInf)/(cutSup-cutInf);
						effErrInter= effErr + (effErrNext - effErr)*(cut-cutInf)/(cutSup-cutInf); 
						effVspt->SetPoint(nEffVspt,pTBinCentre,effInter);
						effVspt->SetPointError(nEffVspt,hsig->GetXaxis()->GetBinWidth(binx)/2,effErrInter);
						//std::cout<<nEffVspt<<" "<<pTBinCentre<<" "<<effInter<<"\n";//-6.0045-11.9865
						nEffVspt=nEffVspt+1;
						break;
					}
				}else if (checkOpt.Contains("rej")){
					double bkgInf = hbkg->Integral(binx, binx, biny, nbinsy+1);
					double bkgSup = hbkg->Integral(binx, binx, biny+1, nbinsy+1);
					double rejInf = 0; double rejSup = 0; double rej = 0;
					double rejErrInf = 0; double rejErrSup = 0; double rejErr = 0;
					if(bkgInf != 0 && bkgSup != 0){
						rejInf = Ibkg / bkgInf;
						rejSup = Ibkg / bkgSup; 
						rejErrInf = sqrt( rejInf * ( rejInf - 1 ) / bkgInf );
						rejErrSup = sqrt( rejSup * ( rejSup - 1 ) / bkgInf );
						rej=rejInf+(rejSup-rejInf)*(cut-cutInf)/(cutSup-cutInf);
						rejErr=rejErrInf+(rejErrSup-rejErrInf)*(cut-cutInf)/(cutSup-cutInf);
						rejVspt->SetPoint(nRejVspt,pTBinCentre,rej);
						rejVspt->SetPointError(nRejVspt,hbkg->GetXaxis()->GetBinWidth(binx)/2,rejErr);
						//std::cout<<nRejVspt<<" "<<pTBinCentre<<" "<<rej<<" +- "<<hbkg->GetXaxis()->GetBinWidth(binx)/2<<"\n";//-6.0045-11.9865
						nRejVspt=nRejVspt+1;
						break;
					}
				}
				break;
			}
		}
	}
	delete cutline;

	// Get rid of the extra points
	if(checkOpt.Contains("eff")){
		for (int i=nbinsx-1;i>=nEffVspt;i--){
			effVspt->RemovePoint(i);
		}
		return effVspt;
		delete effVspt;
	}else if (checkOpt.Contains("rej")){
		for (int i=nbinsx-1;i>=nRejVspt;i--){
			rejVspt->RemovePoint(i);
		}
		return rejVspt;
		delete rejVspt;
	}else{
		std::cout<<"The option should contain either \"eff\" or \"rej\"! Returning null pointer\n";
		return nullptr;
	}
}


// Creates 2D histogram of eta vs pT.
// Also saves the hist, and a ColZ version for easy inspection.
// Can create 2D hists with all entries, or only entries pass certain
// cut values. The cut values should be a TSpline3.
void effMap2D(TH3F* h3D, TString graphName, TDirectory* dir, TSpline3* cutVspt=nullptr){
	TH1* hEtaptb = h3D->Project3D("yx");
	// If cut not specified, get the overall eff map.
	if (cutVspt==nullptr){ 
		TString h3DName=h3D->GetName();
		gStyle->SetPadRightMargin(0.15);
		TCanvas* can2Db = new TCanvas(h3DName+"_yx_ColZ",h3DName+"ColZ", 800 ,600);
		//dir->WriteTObject(h3D);
		dir->WriteTObject(hEtaptb);

		hEtaptb->SetTitle(graphName);
		hEtaptb->Draw("ColZ");
		dir->WriteTObject(can2Db);
		//can2Db->Print(h3DName+"ColZ.png");
		delete can2Db;
	}else{
		int nbinsx = h3D->GetNbinsX();
		int nbinsy = h3D->GetNbinsY();
		int nbinsz = h3D->GetNbinsZ();
		//std::cout<<nbinsx<<" "<<nbinsy<<" "<<nbinsz<<" "<<nbinspT<<" "<<lastpT<<" \n";//pT, eta, tw 42 3 1999//42 25 1999 35 2700 
		//std::cout<<(h3D->GetBin(0,0,1))<<" "<<(h3D->GetBin(0,1,0))<<" "<<(h3D->GetBin(1,0,0))<<"\n";//1188,44,1 returns the global bin number.
		int ptIt=0;  
		double binContent; double pT;
		double cutInf; double cutSup; double cut;
		std::vector<int> cutIndices;

		for (;ptIt<=nbinsx+2;ptIt++){
			pT=h3D->GetXaxis()->GetBinCenter(ptIt);
			cut=cutVspt->Eval(pT);
			int twIt=0; 
			for (;twIt<nbinsz;twIt++){
				cutInf=h3D->GetZaxis()->GetBinLowEdge(twIt);
				cutSup=h3D->GetZaxis()->GetBinUpEdge(twIt);
				if (cutInf<=cut and cutSup>=cut){
					cutIndices.push_back(twIt-1);
					break;
				}
			}
			//binContent=h3D->GetBinContent(ptIt,1,1000);
		}

		// To get a hist with the same binning, clone and wipe
		TH1* effMapAfter=hEtaptb;
		effMapAfter->Reset();
		// Fill the hist
		int etaIt=0;
		double integrated; double eta; int totEntries=0;
		for (ptIt=0;ptIt<=nbinsx+1;ptIt++){
			for (etaIt=0;etaIt<=nbinsy+1;etaIt++){
				integrated=h3D->Integral(ptIt,ptIt,etaIt,etaIt,cutIndices[ptIt],nbinsz+1);
				effMapAfter->SetBinContent(ptIt,etaIt,integrated);
				eta=h3D->GetYaxis()->GetBinUpEdge(etaIt);
				totEntries+=integrated;
			}
		}
		// Print the num. of entries before and after
		int entries3D=h3D->GetEntries();// All entries
		std::cout<<"Total entries: "<<entries3D<<"\nEntries in eff map "<<graphName<<": "<<totEntries<<"\n\n\n";
		// Save the hist and the ColZ
		TCanvas* can2Db = new TCanvas(graphName+"ColZ",graphName+"ColZ", 800 ,600);
		effMapAfter->SetTitle(graphName);
		effMapAfter->SetName(graphName);
		effMapAfter->SetStats(0);
		effMapAfter->Draw("ColZ");
		dir->WriteTObject(effMapAfter);
		dir->WriteTObject(can2Db);
		delete can2Db;
	}
}


TDirectory* buildDir(TFile* outFile, TString dirName){
	TDirectory* dir;
	if (!outFile->GetDirectory(dirName)){// If output file does not have the same directory
		dir = outFile->mkdir(dirName);
	}else{
		dir = outFile->GetDirectory(dirName);
	}
	return dir;
}
TDirectory* buildDir(TDirectory* parentDir, TString daughterDirName){
	TDirectory* dir;
	if(!parentDir->GetDirectory(daughterDirName)){
		dir = parentDir->mkdir(daughterDirName);
	}else{
		dir = parentDir->GetDirectory(daughterDirName);
	}
	return dir;
}




// "main"
void deriveWorkingPoints(TString inputFileName, TString plotOutFileName, TString customCDIfile, TString outTxtName, std::vector<std::string> taggerNames, std::vector<double> fcs, TString jetCollection, std::vector<std::string> WPnames, std::vector<double> WPs, std::string WP_profiles, double minPtForHybridSpline, bool printOut){

	TFile* inf = TFile::Open(inputFileName,"read");// Open the input file
	TFile* plotOuf = TFile::Open(plotOutFileName,"recreate");// Open plot ouput file
	TFile* CDIouf  = TFile::Open(customCDIfile, "recreate");

	// Distinguish wanted WP profiles.
	bool fixedCut;
	bool flatEfficiency;
	bool hybrid;
	bool flatRejectionForWP;
	int len_WP_profiles = WP_profiles.length();
	flatEfficiency = wantProfileOrNot(WP_profiles, len_WP_profiles, "flatEfficiency");
	fixedCut       = wantProfileOrNot(WP_profiles, len_WP_profiles, "fixedCut");
	hybrid         = wantProfileOrNot(WP_profiles, len_WP_profiles, "hybrid");
	flatRejectionForWP = wantProfileOrNot(WP_profiles, len_WP_profiles, "flatRejectionForWP", false);// A hidden option not intending to be used. // Utility: deriving flat rej for the overall rej from a WP

	TString taggerFileOption = "recreate";// For txt file and customCDI file (together with profileFileOption)

	for (int itTagger=0; itTagger<taggerNames.size(); itTagger++){
		TString taggerName = taggerNames[itTagger];
		TString taggerDirName = taggerName;
		//if (taggerName!="MV2c10"){taggerName="DL1r";}
		// Originally taggerName was all lower case, taggerDirName proper
		// name with upper case letters. This is now unnecessary. Kept for
		// convenience, used when the calibration code/AnalysisTop needs
		// more tagger than I have, to create a few "dummy" folders. 
		// taggerName would be the name of the tagger that's actually used,
		// while taggerDirName can be any fake name. 

		saveInTxt(outTxtName, {"\n######################## "+taggerDirName+" ########################\n"}, {}, true, taggerFileOption);// "recreate" if 1st tagger. "update" if not
		saveInTxt(outTxtName, {"# ****** "+jetCollection+" ******\n"}, {}, true);

		TDirectory* taggerDirPlot = buildDir(plotOuf, taggerDirName);
		TDirectory* jetColDirPlot = buildDir(taggerDirPlot, jetCollection);
		TDirectory* taggerDirCDI = buildDir(CDIouf, taggerDirName);
		TDirectory* jetColDirCDI = buildDir(taggerDirCDI, jetCollection);


		// 1D hists for tagger output. Needed by rej plot, and then urejT and crejT needed in flat eff or cut
		TString hnameb("tw"); hnameb += taggerName; hnameb += "_B";
		TString hnamec("tw"); hnamec += taggerName; hnamec += "_C";
		TString hnamel("tw"); hnamel += taggerName; hnamel += "_L";
		TH1F *hb = (TH1F*)inf->Get(hnameb);
		TH1F *hc = (TH1F*)inf->Get(hnamec);
		TH1F *hl = (TH1F*)inf->Get(hnamel);
		// These two needed in flat eff, flat cut, rej vs eff
		WorkingPoint* urejT = new WorkingPoint(hb, hl);
		WorkingPoint* crejT = new WorkingPoint(hb, hc);
		// Saves the two rejection plots in the output
		jetColDirPlot->WriteTObject(urejT->cloneRejVsEff("urejvseff"+taggerDirName));
		jetColDirPlot->WriteTObject(crejT->cloneRejVsEff("crejvseff"+taggerDirName));


		// Used in urejTpt&crejTpt thus flat Eff and flat cut. hbpt and hlpt in urejTpt thus in hybrid too
		hnameb = "twpt"; hnameb += taggerName; hnameb += "_B";// Needed in flatEff and hybrid
		hnamec = "twpt"; hnamec += taggerName; hnamec += "_C";
		hnamel = "twpt"; hnamel += taggerName; hnamel += "_L";
		TH2F* hbpt = (TH2F*)inf->Get(hnameb);// Used in hybrid. Originally TH1F
		TH2F* hcpt = (TH2F*)inf->Get(hnamec);
		TH2F* hlpt = (TH2F*)inf->Get(hnamel);// Used in hybrid

		// Get the eff map before applying cuts
		//jetColDirPlot->WriteTObject(hbpt);
		hnameb = "twetapt"; hnameb += taggerName; hnameb += "_B";
		TH3F* h3Db = (TH3F*)inf->Get(hnameb);
		effMap2D(h3Db, "effMapEtapt", jetColDirPlot);


		// The following block is a cross-check for eff map after cut:
		// make a cut that allows all entries, see if the resulting hist
		// is the same as simply projecting the 3D one.
		bool testEffMap = false;
		if (testEffMap==true){
			double zLow = h3Db->GetZaxis()->GetBinLowEdge(0);
			std::cout<<"Lowest tw: "<<zLow<<" "<<"\n";
			int nptBins = hbpt->GetNbinsX();
			TGraphErrors* testCutLine = new TGraphErrors(nptBins);
			int ni = 0;
			for (; ni<nptBins; ni++){
				double pTValues = hbpt->GetXaxis()->GetBinCenter(ni);
				testCutLine->SetPoint(ni,pTValues,zLow);//-6-12//just any low number doesn't work. segnemtation violation since the vector has no entries.
			}
			////testCutLine->Print("all");
			////std::cout<<"Above is the cut line.\n";
			TSpline3* testCutLineSp = new TSpline3("testCutLineSpline", testCutLine);
			effMap2D(h3Db, "effMapTest", jetColDirPlot,testCutLineSp);
			delete testCutLine;
			delete testCutLineSp;
		}


		for (int itWP = 0; itWP<WPnames.size(); itWP++){
			TString opName = WPnames[itWP];

			// ------------------ flat rejection WP  ------------------
			if (opName.Contains("c") or opName.Contains("C") or opName.Contains("l") or opName.Contains("L") or opName.Contains("u") or opName.Contains("U")){

				saveInTxt(outTxtName, {"------------ flatRejection "+WPnames[itWP]+" ------------\n# Tagger: ",taggerName,"\n"}, {}, true);
				TDirectory* opDirPlot = jetColDirPlot->mkdir(opName);
				TDirectory* opDirCDI = jetColDirCDI->mkdir("FlatRej_"+opName);

				// This constructor already derives the cut vs pT
				// and eff vs pT. Then just get them and write them in the
				// output file that contains plots.
				WorkingPoint* flatRejTool;
				if (opName.Contains("c") or opName.Contains("C")){
					flatRejTool = new WorkingPoint(hbpt, hcpt, WPs[itWP]);
				}else{
					flatRejTool = new WorkingPoint(hbpt, hlpt, WPs[itWP]);
				}

				TGraphErrors* cutVspt = (TGraphErrors*)flatRejTool->getCutVsX_flatRej();
				TGraphErrors* effVspt = (TGraphErrors*)flatRejTool->getEffVsX_flatRej("effvspt"+opName+taggerDirName);
				opDirPlot->WriteTObject(cutVspt->Clone("cutvspt"+opName+taggerDirName));
				opDirPlot->WriteTObject(effVspt->Clone("effvspt"+opName+taggerDirName));
				// Write in customCDI and txt. Printout.
				writeTSpline3(opDirCDI, cutVspt, "cutvalues");
				writeTVector(opDirCDI, fcs[itTagger], "fraction");
				saveInTxt(outTxtName, cutVspt, "pT", "cut value");
				if (printOut==true){
					std::cout<<"The cut values for flat rejection"+opName+": \n";
					cutVspt->Print("all");
				}
				// Cross-check
				TGraphErrors* rejVsptCheck_Spline;
				if (opName.Contains("c") or opName.Contains("C")){
					rejVsptCheck_Spline = check2D_Spline(cutVspt, hbpt, hcpt, "rejVsptCheck_fromSpline", "rej",false);
				}else{
					rejVsptCheck_Spline = check2D_Spline(cutVspt, hbpt, hlpt, "rejVsptCheck_fromSpline", "rej",false);
				}
				opDirPlot->WriteTObject(rejVsptCheck_Spline);
				// Eff map
				TSpline3* cutVsptSp = new TSpline3("cutVsptSpline", cutVspt);
				effMap2D(h3Db, "effMapFlatRej", opDirPlot, cutVsptSp);
				delete flatRejTool;
				delete cutVsptSp;


			}else{
				// ------------------ non flat rej WP  ------------------
				saveInTxt(outTxtName, {"------------------ "+WPnames[itWP]+" ------------------\n# Tagger: ",taggerName,"\n"}, {}, true);
				TDirectory *opDirPlot = jetColDirPlot->mkdir("OP"+opName);

				// uw, ur needed in urejTpt thus flatcut_effProfile thus hybrid.
				double ur, urerr, uw, uwerr;// uw: cut, ur: light jet rej
				double cr, crerr, cw, cwerr;
				// This method gives ur, urerr, uw, ... values
				urejT->getRej(WPs[itWP], ur, urerr, uw, uwerr);
				crejT->getRej(WPs[itWP], cr, crerr, cw, cwerr);
				saveInTxt(outTxtName, {"C rejection:     ","	+- ","\n"}, {cr,crerr}, 1);
				saveInTxt(outTxtName, {"Light rejection: ","	+- ","\nCut (tagger score): ","	+- ","\n\n"}, {ur,urerr,uw,uwerr}, 1);


				// Needed in flat Eff and flat cut. urejTpt in hybrid too.
				WorkingPoint* urejTpt = new WorkingPoint(hbpt, hlpt, WPs[itWP], uw, ur);
				WorkingPoint* crejTpt = new WorkingPoint(hbpt, hcpt, WPs[itWP], cw, cr);


				////////////////// flatEfficiency //////////////////
				if (flatEfficiency==true){
					saveInTxt(outTxtName, {"# ------ flatEfficiency ------\n# Tagger: ",taggerDirName,". Working point (efficiency): ",opName,"\n"}, {}, true, "update", "# ------ flatEfficiency ------\n");
					TDirectory* flateffdir = opDirPlot->mkdir("FlatBEff");
					TDirectory* opDirCDI = jetColDirCDI->mkdir("FlatEff_"+opName);
					// Write rej vs pT, cut vs pT in the plot output file
					flateffdir->WriteTObject( urejTpt->getRejVsX()->Clone("urejvspt"+opName+taggerDirName) );
					flateffdir->WriteTObject( crejTpt->getRejVsX()->Clone("crejvspt"+opName+taggerDirName) );
					TGraphErrors* cutVspt = (TGraphErrors*) urejTpt->getCutVsX();// TGraphErrors* makes it possible to inspect it by plotting.
					flateffdir->WriteTObject( cutVspt->Clone("cutvspt"+opName+taggerDirName) );
					// Tried saving spline in the plot file instead, but can't easily inspect. Thus didn't do it.

					// write in custom CDI file, txt file, print out
					writeTSpline3(opDirCDI, cutVspt);
					writeTVector(opDirCDI, fcs[itTagger], "fraction");
					saveInTxt(outTxtName, cutVspt, "pT", "cut value");
					if (printOut==true){
						std::cout<<"The cut values: \n";
						cutVspt->Print("all");
					}
					// Cross-check
					TGraphErrors* effVsptCheck_Spline = check2D_Spline(cutVspt, hbpt, hcpt, "effVsptCheck_fromSpline", "eff", false);
					flateffdir->WriteTObject(effVsptCheck_Spline);
					// Eff map
					TSpline3* cutVsptSp = new TSpline3("cutVsptSpline", cutVspt);
					effMap2D(h3Db, "effMapFlatEff", flateffdir, cutVsptSp);
					delete cutVsptSp;
				}


				////////////////// flatRejectionForWP //////////////////
				if (flatRejectionForWP==true){
					saveInTxt(outTxtName, {"# ------ flatRejectionForWP ------\n# Tagger: ",taggerDirName,". Working point (efficiency): ",opName,"\n# Flat c rejection\n"}, {}, true, "update", "# ------ flatRejectionForWP ------\n");
					TDirectory *flatRejdir = opDirPlot->mkdir("FlatRej");
					TString opDirNameC = "FlatCRej_"+to_string(cr);
					TString opDirNameU = "FlatURej_"+to_string(ur);
					TDirectory* opDirCDIC = jetColDirCDI->mkdir(opDirNameC);
					TDirectory* opDirCDIU = jetColDirCDI->mkdir(opDirNameU);
	
					// Uses constructor for flat rej
					// Write in plot output file
					WorkingPoint* flatCRejForWP = new WorkingPoint(hbpt, hcpt, cr);
					WorkingPoint* flatURejForWP = new WorkingPoint(hbpt, hlpt, ur);
					TGraphErrors* cCutVspt = (TGraphErrors*)flatCRejForWP->getCutVsX_flatRej();
					TGraphErrors* uCutVspt = (TGraphErrors*)flatURejForWP->getCutVsX_flatRej();
					TGraphErrors* cEffVspt = (TGraphErrors*)flatCRejForWP->getEffVsX_flatRej("effvsptflatc"+opName+taggerDirName);
					TGraphErrors* uEffVspt = (TGraphErrors*)flatURejForWP->getEffVsX_flatRej("effvsptflatu"+opName+taggerDirName);
					flatRejdir->WriteTObject( cCutVspt->Clone("cutvsptflatc"+opName+taggerDirName) );
					flatRejdir->WriteTObject( uCutVspt->Clone("cutvsptflatu"+opName+taggerDirName) );
					flatRejdir->WriteTObject( cEffVspt->Clone("effvsptflatc"+opName+taggerDirName) );
					flatRejdir->WriteTObject( uEffVspt->Clone("effvsptflatu"+opName+taggerDirName) );

					// Write in custom CDI (root and txt), print out
					writeTSpline3(opDirCDIC, cCutVspt, "cutvaluesFlatCrej");
					writeTSpline3(opDirCDIU, uCutVspt, "cutvaluesFlatUrej");
					writeTVector(opDirCDIC, fcs[itTagger], "fraction");
					writeTVector(opDirCDIU, fcs[itTagger], "fraction");

					saveInTxt(outTxtName, cCutVspt, "pT", "cut value");
					saveInTxt(outTxtName, {"# Flat u rejection \n"}, {}, printOut, "");
					saveInTxt(outTxtName, uCutVspt, "pT", "cut value");

					if (printOut==true){
						std::cout<<"\nThe cut values for flat c rejection: \n";
						cCutVspt->Print("all");
						std::cout<<"The cut values for flat u rejection: \n";
						uCutVspt->Print("all");
					}

					// Cross-check
					TGraphErrors* cRejVsptCheck_Spline = check2D_Spline(cCutVspt, hbpt, hcpt, "cRejVsptCheck_fromSpline", "rej", false);
					flatRejdir->WriteTObject(cRejVsptCheck_Spline);
					TGraphErrors* uRejVsptCheck_Spline = check2D_Spline(uCutVspt, hbpt, hlpt, "uRejVsptCheck_fromSpline", "rej", false);
					flatRejdir->WriteTObject(uRejVsptCheck_Spline);
					// Eff map
					TSpline3* cCutVsptSp = new TSpline3("cCutVsptSpline", cCutVspt);
					effMap2D(h3Db, "effMapFlatCrej", flatRejdir, cCutVsptSp);
					TSpline3* uCutVsptSp = new TSpline3("uCutVsptSpline", uCutVspt);
					effMap2D(h3Db, "effMapFlatUrej", flatRejdir, uCutVsptSp);
					delete cCutVsptSp;
					delete uCutVsptSp;
				}


				if (fixedCut==true or hybrid==true){
					TGraphErrors* flatcut_effProfile = (TGraphErrors*)urejTpt->getEffFromCutVsX()->Clone("efffromcutvspt"+opName+taggerDirName);// Needed in fixed cut and hybrid//TGraph should be sufficient

					////////////////// fixedCut //////////////////
					if (fixedCut==true){// Needs urejTpt.
						saveInTxt(outTxtName, {"# ------ fixedCut ------\n"}, {}, true, "# ------ fixedCut ------\n");
						saveInTxt(outTxtName, {"# Tagger: "+taggerDirName+". Cut value for working point "+opName+":\n","\n"}, {uw}, printOut);
						TDirectory* flatcutdir = opDirPlot->mkdir("FixedCut");
						TDirectory* opDirCDI = jetColDirCDI->mkdir("FixedCutBEff_"+opName);

						// Get and write in plot output file
						flatcutdir->WriteTObject( urejTpt->getRejFromCutVsX()->Clone("urejfromcutvspt"+opName+taggerDirName) );
						flatcutdir->WriteTObject( crejTpt->getRejFromCutVsX()->Clone("crejfromcutvspt"+opName+taggerDirName) );
						flatcutdir->WriteTObject( flatcut_effProfile );

						// Write in custom CDI
						writeTVector(opDirCDI, uw, "cutvalue");
						writeTVector(opDirCDI, fcs[itTagger], "fraction");

						// Cross-check
						std::string crossCheckFixedCut=check1D(uw, hb, hc);
						saveInTxt(outTxtName, {"# Cross-check: the b efficiency from this cut value is: "+crossCheckFixedCut+"\n"}, {}, true);
						saveInTxt(outTxtName, {"\n\n"}, {}, false);

						// Make a flat cut line to make eff map
						int nptBins = hbpt->GetNbinsX();
						TGraphErrors* cutLine = new TGraphErrors(nptBins);
						int ni = 0;
						for (; ni<nptBins; ni++){
							double pTValues = hbpt->GetXaxis()->GetBinCenter(ni);
							cutLine->SetPoint(ni, pTValues, uw);
						}
						//cutLine->Print("all");
						// Eff map
						TSpline3* cutSp = new TSpline3("cutSpline", cutLine);
						effMap2D(h3Db, "effMapFixedCut", flatcutdir, cutSp);
						delete cutLine;
						delete cutSp;
					}


					////////////////// Hybrid //////////////////
					// This part needs: 
					// flatcut_effProfile //from urejTpt
					// WPs[itWP]
					// hbpt, hlpt
					if (hybrid==true){
						TString theTextHybrid = "# ------ hybrid ------\n# Tagger: "+taggerDirName+". Working point (efficiency): "+opName+". minPtForHybridSpline: ";
						saveInTxt(outTxtName, {theTextHybrid,"\n"}, {minPtForHybridSpline}, printOut, "update", "# ------ hybrid ------\n");
						TDirectory* hybeffdir = opDirPlot->mkdir("HybBEff");
						TDirectory* opDirCDI = jetColDirCDI->mkdir("HybBEff_"+opName);

						// Builds the hybrid profile, 
						// uses the constructor for hybrid,
						// writes cut vs pT
						// (TGraphErrors*) parentheses are type conversion (from TGraph*)
						TGraphErrors* hybEffProfile = (TGraphErrors*)hybridProfileBuilder( flatcut_effProfile, WPs[itWP] );// The eff goes to our specified eff after specified condition in hybridProfileBuilder
						//std::cout<<"The efficiency vs pt: \n";
						//hybEffProfile->Print("all");
						WorkingPoint* urejTptdl1 = new WorkingPoint(hbpt, hlpt, hybEffProfile);
						WorkingPoint* crejTptdl1 = new WorkingPoint(hbpt, hcpt, hybEffProfile);

						TGraphErrors* hybrid_cut_profile = (TGraphErrors*)urejTptdl1->getCutVsX()->Clone("cutvspt"+opName+taggerDirName);// TGraph could be sufficient?
						hybeffdir->WriteTObject( urejTptdl1->getRejVsX()->Clone("urejvspt"+opName+taggerDirName) );
						hybeffdir->WriteTObject( crejTptdl1->getRejVsX()->Clone("crejvspt"+opName+taggerDirName) );
						hybeffdir->WriteTObject( hybEffProfile );
						hybeffdir->WriteTObject( hybrid_cut_profile );

						// Write in custom CDI, printout
						// This line has to be before WriteTObject
						// because it actually changes the cut values
						writeHybridWPFile(opDirCDI, hybrid_cut_profile, minPtForHybridSpline);
						writeTVector(opDirCDI, fcs[itTagger], "fraction");
						saveInTxt(outTxtName, hybrid_cut_profile, "pT", "cut value");
						if (printOut==true){
							std::cout<<"The cut values for hybrid profile: \n";
							hybrid_cut_profile->Print("all");
						}
						// Cross-check
						TGraphErrors* effVsptCheck_Spline = check2D_Spline(hybrid_cut_profile, hbpt, hcpt, "effVsptCheck_fromSpline", "eff", false);
						hybeffdir->WriteTObject(effVsptCheck_Spline);
						// Eff map
						TSpline3* hybridSp = new TSpline3("hybrid_cut_profile_Spline", hybrid_cut_profile);
						effMap2D(h3Db, "effMapHybrid", hybeffdir, hybridSp);
						delete urejTptdl1;
						delete crejTptdl1;
						delete hybridSp;
					}// if hybrid
				}// if cut or hybrid
				delete urejTpt;
				delete crejTpt;
			}// if flat rej or WP
		}// for WPs
		taggerFileOption="update";
		delete urejT;
		delete crejT;
	}// for taggers
	plotOuf->Close();// Must close the opened file. Then clear the memory
	CDIouf->Close();
	delete plotOuf;
	delete CDIouf;
}



