import ROOT
import os

ROOT.gROOT.SetBatch(1)

# Yusong: to use .json file
from math import sqrt
import argparse,os,sys,json
import ROOT

parser =  argparse.ArgumentParser(description='Make Plots')
parser.add_argument("-c", "--config", dest="c", type=str, required=True)
opt = parser.parse_args()

print 'Reading json... File name: ', opt.c
conf_file = open(opt.c, "r")
conf = json.load(conf_file)
# Yusong: the part using .json here ends here.

plotOutputFile="../"+str(conf.get("plotOutFileName"))
customCDIfile="../"+str(conf.get("customCDIfile"))

infile = ROOT.TFile(plotOutputFile,'read')#Y.T. the input file is originally: '../WP_Plots.root'

taggerList=conf.get("taggers")
taggers=[]
for n in range(len(taggerList)):
	if type(taggerList[n])==list:
		taggers.append(str(taggerList[n][0]))
	else:
		taggers.append(str(taggerList[n]))

jetCollection=str(conf.get("jetCollection"))

#originally '../WP_Plots.root','read''
#infile = ROOT.TFile('WP_Plots.root','read')

'''
# I commented out some folder manipulation.
if not os.path.exists('Plots'):
	os.mkdir('Plots')
os.chdir('Plots')
'''
######### ROC Curves #########
if not os.path.exists('ROC_curves'):
	os.mkdir('ROC_curves')
#os.chdir('..')

for tagger in taggers:
	print tagger
	c = ROOT.TCanvas('c','c',800,800)
	leg = ROOT.TLegend(0.5,0.8,0.9,0.9)


	urejVsEff = infile.Get(tagger+'/'+jetCollection+'/urejvseff'+tagger)
	crejVsEff = infile.Get(tagger+'/'+jetCollection+'/crejvseff'+tagger)

	crejVsEff.SetLineWidth(2)
	urejVsEff.SetLineWidth(2)

	crejVsEff.SetLineColor(ROOT.kRed)
	ROOT.gPad.SetLogy(1)

	mg = ROOT.TMultiGraph()
	mg.Add(urejVsEff)
	mg.Add(crejVsEff)
	mg.SetTitle(jetCollection+' '+tagger+' rejection')

	mg.Draw('ALX')

	mg.GetHistogram().GetXaxis().SetRangeUser(0.4,1)
	mg.GetHistogram().GetYaxis().SetRangeUser(1,100000)

	mg.GetHistogram().GetYaxis().SetTitle('rejection')
	mg.GetHistogram().GetXaxis().SetTitle('b efficiency')

	ROOT.gPad.Modified()
	ROOT.gPad.Update();

	leg.AddEntry(urejVsEff,'light rejection','L')
	leg.AddEntry(crejVsEff,'c rejection','L')

	leg.Draw('same')
	c.Print('ROC_curves/'+jetCollection+'_'+tagger+'_rejection.png')
	#Yusong: was 'Plots/ROC_curves/'+jetCollection+'_'+tagger+'_rejection.png'

