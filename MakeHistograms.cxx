#include <iostream>
#include <TH3F.h>
#include <time.h>


// These have to be before the function "prepareBins" since the later uses some of them.
void printVector(std::vector<double> toPrint, TString printBefore=""){
	std::cout<<printBefore;
	for (double itPrintVector:toPrint){
		std::cout<<itPrintVector<<" ";
	}
	std::cout<<"\n";
}
// TString and std::string aren't exchangeable at any direction. (When propagating variables between functions.)
void printVector(std::vector<TString> toPrint, TString printBefore=""){
	std::cout<<printBefore;
	for (TString itPrintVector:toPrint){
		std::cout<<itPrintVector<<" ";
	}
	std::cout<<"\n";
}
void printVector(std::vector<std::string> toPrint, TString printBefore=""){
	std::cout<<printBefore;
	for (std::string itPrintVector:toPrint){
		std::cout<<itPrintVector<<" ";
	}
	std::cout<<"\n";
}


void prepareBins(std::vector<double>* binVector, double minEdge, double maxEdge, bool firstTagger, TString toPrint="", bool printOut=0){
	int nBins=binVector->size();
	std::cout<<toPrint;
	if (nBins!=1 and firstTagger){// If it's the 1st tagger and has more than 1 elements, it's user specified.
		if (printOut){
			printVector(*binVector);
			std::cout<<"	Num. of bins: "<<nBins-1<<"\n";
		}
	}else{
		if (firstTagger){
			nBins=int((*binVector)[0]);
		}else{nBins=nBins-1;}
		(*binVector)={};// Clear up the vector
		// Evenly disbribute the bins. Used <= instead of < because the num. of bin edges=num of bins+1.
		for (int itPB=0; itPB<=nBins; itPB++){
			(*binVector).push_back( minEdge+(maxEdge-minEdge)*(float(itPB)/float(nBins)) );
		}
		if (printOut){printVector(*binVector);}
		std::cout<<"	Num. of bins: "<<binVector->size()-1<<"\n";
	}
}


void MakeHists(TTree *inTree, TString oufname, TString jetCollection, TString branchName, std::vector<std::string> jetParameterNames, TString flagLeafName1, TString flagLeafName2, std::vector<std::vector<std::string>> taggerLeafNames, TString cutString, std::vector<double> ptBins, std::vector<double> etaBins, std::vector<double> tDBins){

	std::vector<TString> taggerNames;
	std::vector<TString> taggerDs;// Tagger discriminants
	for (std::vector<std::string> taggerLeafName:taggerLeafNames){
		taggerNames.push_back(taggerLeafName[0]);
		taggerDs.push_back(taggerLeafName[1]);
	}
	TString jet_pt=jetParameterNames[0]; TString jet_eta=jetParameterNames[1];

	std::vector<TString> labels;
	std::vector<TString> flagValueCuts;
	std::vector<TString> truthLabels;
	labels.push_back("B"); flagValueCuts.push_back("==5"); truthLabels.push_back(flagLeafName1);
	labels.push_back("C"); flagValueCuts.push_back("==4"); truthLabels.push_back(flagLeafName1);
	labels.push_back("L"); flagValueCuts.push_back("<4");  truthLabels.push_back(flagLeafName1);

	if (flagLeafName2!=""){
		labels.push_back("T");  flagValueCuts.push_back("==15"); truthLabels.push_back(flagLeafName1);
		labels.push_back("BB"); flagValueCuts.push_back("==55"); truthLabels.push_back(flagLeafName2);
		labels.push_back("BC"); flagValueCuts.push_back("==54"); truthLabels.push_back(flagLeafName2);
		labels.push_back("CC"); flagValueCuts.push_back("==44"); truthLabels.push_back(flagLeafName2);
		labels.push_back("B0"); flagValueCuts.push_back("==5");  truthLabels.push_back(flagLeafName2);
		labels.push_back("C0"); flagValueCuts.push_back("==4");  truthLabels.push_back(flagLeafName2);
		labels.push_back("L0"); flagValueCuts.push_back("<4");   truthLabels.push_back(flagLeafName2);
	}

	bool firstTagger=1;
	TFile *outf=TFile::Open(oufname, "recreate");
	for (int it=0;it<taggerDs.size();it++){

		std::cout<<"\nWorking on "<<taggerNames[it]<<". Tagger leaf name/discriminant: "<<taggerDs[it]<<std::endl;
		// Got the following min/max values here: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/FTAG-2018-01/
		// These values have to be exactly the possible range, i.e. no spare edges on the histogram. Bcause if specify e.g. 1.1 to leave an edge on the hist, the derived cut vs pt values might be unphysical.
		double minEdge = -1.0;//if( taggerNames[it].Contains("mv2") ){
		double maxEdge = 1.0;
		if (taggerNames[it].Contains("DL1") or taggerNames[it].Contains("dl1")){
			minEdge = -6;
			maxEdge = 12;
		}
		std::cout<<"Minimum tagger discriminant value: "<<minEdge<<". Maximum: "<<maxEdge<<".\n";
		if (firstTagger){
			prepareBins(&ptBins, 10.0, 3000.0, firstTagger, "pT bins: ",1);
			prepareBins(&etaBins, 0.0, 2.5, firstTagger, "eta bins: ",1);
			prepareBins(&tDBins, minEdge, maxEdge, firstTagger, "Tagger output bins: ");
			firstTagger=0;
		}else{
			prepareBins(&tDBins, minEdge, maxEdge, firstTagger);
		}
		int nptBin=ptBins.size(); 
		int netaBin=etaBins.size();
		int ntDBin=tDBins.size();
		//std::cout<<"nptBin "<<nptBin<<" netaBin "<<netaBin<<" ntDBin "<<ntDBin<<"\n";
		
		TString taggerNamesTS=taggerNames[it];
		TString taggerDsTS=taggerDs[it];
		for (int il=0;il<labels.size();il++){
			TH3F *hist3D=new TH3F("twetapt"+taggerNames[it]+"_"+labels[il], "twetapt"+taggerNames[it]+labels[il],
			nptBin-1,	&ptBins[0],
			netaBin-1,	&etaBins[0],
			ntDBin-1,	&tDBins[0]);

			int nproj=inTree->Project("twetapt"+taggerNamesTS+"_"+labels[il], 
				taggerDsTS+":abs("+jet_eta+"):"+jet_pt+"/1000", 
				cutString+"abs("+truthLabels[il]+")"+flagValueCuts[il]);
			// Project() https://root.cern.ch/doc/master/classTTree.html
			//std::cout<<typeid(inTree).name()<<std::endl;//P5TTree
			hist3D->Write();


			TH2F *hist2D=new TH2F("twpt"+taggerNames[it]+"_"+labels[il], "twpt"+taggerNames[it]+labels[il],
			nptBin-1, &ptBins[0],
			ntDBin-1, &tDBins[0]);

			nproj=inTree->Project("twpt"+taggerNamesTS+"_"+labels[il],
				taggerDsTS+":"+jet_pt+"/1000",
				cutString+"abs("+truthLabels[il]+")"+flagValueCuts[il]);
			hist2D->Write();


			TH1F *hist1D=new TH1F("tw"+taggerNames[it]+"_"+labels[il], "tw"+taggerDs[it]+labels[il], ntDBin-1, &tDBins[0]);

			nproj=inTree->Project("tw"+taggerNamesTS+"_"+labels[il],
				taggerDsTS,
				cutString+"abs("+truthLabels[il]+")"+flagValueCuts[il]);
			hist1D->Write();
			delete hist1D;
			delete hist2D;
			delete hist3D;
		}
	}
	outf->Close();
}


// "main"
void MakeHistograms(TString inFileOrDir, TString outFile, TString jetCollection, TString branchName, std::vector<std::string> jetParameterNames, TString flagLeafName1, TString flagLeafName2, std::vector<std::vector<std::string>> taggerLeafNames, TString cutString, std::vector<double> ptBinOrNum, std::vector<double> etaBinOrNum, std::vector<double> tDBinOrNum){

	clock_t tStart=clock();// To count the time.

	// Printout.
	std::cout<<"\nInput folder/file: "<<inFileOrDir<<"\n";//std::endl separates line and flushes. No need.
	if (!outFile.EndsWith(".root")){// Check that the output file ends with ".root"
		std::cout<<"The specified output file does not end with \".root\". Apending \".root\" automatically.\n";
		outFile=outFile+".root";
		std::cout<<"Output file: "<<outFile<<"\n";
	}else{std::cout<<"Output file: "<<outFile<<"\n";}
	std::cout<<"jetCollection: "<<jetCollection<<"\n";
	std::cout<<"branchName: "<<branchName<<"\n";
	printVector(jetParameterNames,"jetParameterNames: ");
	std::cout<<"flagLeafName1: "<<flagLeafName1<<"\n";
	if (flagLeafName2!=""){std::cout<<"flagLeafName2: "<<flagLeafName2<<"\n";}
	std::cout<<"taggerLeafNames: \n";
	for (std::vector<std::string> i:taggerLeafNames){
		printVector(i,"	");
	}
	std::cout<<"cutString: "<<cutString<<"\n\n";

	// Could add a check on whether the inFileOrDir Is actually a directory. Doesn't work yet. Cannot use IsDirectory() because it's for TSystemFile. Here it's a TString or later a TFile*.
	//if (!inFileOrDir.IsDirectory() && inFileOrDir.EndsWith(".root")){//std::cout<<"inf.IsDirectory(): "<<inf.IsDirectory()<<std::endl;
	if (inFileOrDir.EndsWith(".root")){
		// In future could add: read root files in the subcirectories.
		TFile *inf=TFile::Open(inFileOrDir,"READ");
		TTree *intree = (TTree*)inf->Get(branchName);
		//std::cout<<"intree->GetEntries(): "<<intree->GetEntries()<<std::endl;
		MakeHists(intree, outFile, jetCollection, branchName, jetParameterNames, flagLeafName1, flagLeafName2, taggerLeafNames, cutString, ptBinOrNum, etaBinOrNum, tDBinOrNum);
	}else{
		//std::cout<<"The input file is not a root file. Hope it is a directory."<<std::endl;
		if (!inFileOrDir.EndsWith("/")){
			//inFileOrDir.Remove(inFileOrDir.Length()-1);
			inFileOrDir=inFileOrDir+"/";
		}
		TChain* myChain=new TChain(branchName);// Future could add new function: if different directory structure.
		TSystemDirectory myinDirectory(inFileOrDir,inFileOrDir);// In root reference guide: (const char* dirname,const char* path )
		TList *files=myinDirectory.GetListOfFiles();
		//(*files).Print(); // Output: Collection name='TList', class='TList', size=5  OBJ: TSystemDirectory.	/afs/cern.ch/user/t/tiany/FTAG_WorkingPoints-master/ntuples/ and then .. and files in the folder "ntuples"

		TSystemFile *file;
		TString fname;
		TIter next(files); 
		while ((file=(TSystemFile*)next())) {// need the extra pair of parentheses. Otherwise warning: using the result of an assignment as a condition without parentheses
			//(*file).Print();// Output: OBJ: TSystemFile	user.bdong.16276417.Akt4EMPf._000001.root	/afs/cern.ch/user/t/tiany/FTAG_WorkingPoints-master/ntuples/  ., .., and the root files.
			fname=file->GetName();
			if (!file->IsDirectory() && fname.EndsWith(".root")) {// The name of a directory can end with ".root" as well, thus the "IsDirectory" requirement.
				myChain->Add(inFileOrDir+fname);
				std::cout<<"File: "<<fname<<", added.\n";
			}
		}
		if (myChain->GetEntries()){
			MakeHists(myChain, outFile, jetCollection, branchName, jetParameterNames, flagLeafName1, flagLeafName2, taggerLeafNames, cutString, ptBinOrNum, etaBinOrNum, tDBinOrNum);
		}else{
			std::cout<<"There are no entries in the TChain. Maybe there are no .root files in this folder?\n";
		} 
		delete myChain;// Memory leak
	} 
	std::cout<<"Time taken: "<<double(clock() - tStart)/CLOCKS_PER_SEC<<"s\n";
}



