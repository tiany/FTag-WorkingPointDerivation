# Yusong: this and the other plotting code doesn't change the output files from stage 2.

import ROOT
import os

ROOT.gROOT.SetBatch(1)#Yusong: ?

# Yusong: to use .json file
from math import sqrt
import argparse,os,sys,json
import ROOT

parser =  argparse.ArgumentParser(description='Make Plots')
parser.add_argument("-c", "--config", dest="c", type=str, required=True)
opt = parser.parse_args()

print 'Reading json... File name: ', opt.c
conf_file = open(opt.c, "r")
conf = json.load(conf_file)
# Yusong: the part using .json here ends here.


plotOutputFile="../"+str(conf.get("plotOutFileName"))
customCDIfile="../"+str(conf.get("customCDIfile"))

infile = ROOT.TFile(plotOutputFile,'read')
inCutFile = ROOT.TFile(customCDIfile)


logXaxis = False

taggerList=conf.get("taggers")
taggers=[]
for n in range(len(taggerList)):
	if type(taggerList[n])==list:
		taggers.append(str(taggerList[n][0]))
	else:
		taggers.append(str(taggerList[n]))

jetCollection=str(conf.get("jetCollection"))

OPlist=conf.get("operatingPoints")
operatingPoints=[]
for n in range(len(OPlist)):
	if type(OPlist[n])!=list:
		operatingPoints.append( [OPlist[n]/100.,str(OPlist[n])] )
print operatingPoints

'''
if not os.path.exists('Plots'):
	os.mkdir('Plots')
os.chdir('Plots')#Enter the folder "Plots"
'''

WP_profiles=conf.get("WP_profiles")
for n in range(len(WP_profiles)):
	WP_profiles[n]=str(WP_profiles[n])
WP_profiles=str(WP_profiles)

if WP_profiles.find("hybrid")==-1:
	print "There is no hybrid profile. Then this code isn't useful for you because it plots graphs related to hybrid profile."
else:
	print "Making graphs related to hybrid profile."
	if not os.path.exists('HybridCuts'):#Y.T. In the current folder, make a folder called his.
		os.mkdir('HybridCuts')
	#os.chdir('..')# Y.T. go to the previous folder. I commented out these folder manipulations
	for tagger in taggers:
		for op in operatingPoints:
			c = ROOT.TCanvas('c','c',1600,800)
			c.Divide(2)

			c.cd(1)
			if logXaxis:
				ROOT.gPad.SetLogx(1)
			leg = ROOT.TLegend(0.5,0.8,0.9,0.9)

			hybrid_spline_cut = inCutFile.Get(tagger+'/'+jetCollection+'/HybBEff_'+op[1]+'/cutvalues')
			hybrid_cut_Profile = infile.Get(tagger+'/'+jetCollection+'/OP'+op[1]+'/HybBEff/cutvspt'+op[1]+tagger)

			# fixedCut_cut = inCutFile.Get(tagger+'/'+jetCollection+'/FixedCutBEff_'+op[1]+'/cutvalue')

			# xMin = hybrid_cut_Profile.GetXaxis().GetXmin()
			# xMax = hybrid_cut_Profile.GetXaxis().GetXmax()

			# fixedCutLine = ROOT.TLine(xMin,fixedCut_cut[0],xMax,fixedCut_cut[0])

			#print 'cut value ',fixedCut_cut[0]

			#print tagger+'/'+jetCollection+'/OP'+op[1]+'/FlatBEff/cutvspt'+op[1]+tagger
			flat_eff_cut_profile = infile.Get(tagger+'/'+jetCollection+'/OP'+op[1]+'/FlatBEff/cutvspt'+op[1]+tagger)

			hybrid_cut_Profile.SetLineWidth(4)
			flat_eff_cut_profile.SetLineWidth(3)
			hybrid_spline_cut.SetLineWidth(2)

			hybrid_cut_Profile.SetLineStyle(1)
			flat_eff_cut_profile.SetLineStyle(9)
			hybrid_spline_cut.SetLineStyle(2)

			hybrid_cut_Profile.SetMarkerStyle(3)
			flat_eff_cut_profile.SetMarkerStyle(7)
			hybrid_spline_cut.SetMarkerStyle(6)

			hybrid_cut_Profile.SetMarkerSize(10)
			flat_eff_cut_profile.SetMarkerSize(100)
			hybrid_spline_cut.SetMarkerSize(20)

			#hybrid_cut_Profile.SetLineColor(ROOT.kRed)
			flat_eff_cut_profile.SetLineColor(ROOT.kRed)#ROOT.kMagenta)
			hybrid_spline_cut.SetLineColor(ROOT.kMagenta)

			mg = ROOT.TMultiGraph()#Yusong: mg: multigraph
			mg.Add(hybrid_cut_Profile)
			mg.Add(flat_eff_cut_profile)
			mg.SetTitle(jetCollection+' '+tagger+' OP'+op[1]+' cut profiles')
			mg.Draw('ALX')

			hybrid_spline_cut.Draw('same')
			mg.GetHistogram().GetYaxis().SetTitleOffset(1.4)
			mg.GetHistogram().GetYaxis().SetTitle('cut value')
			mg.GetHistogram().GetXaxis().SetTitle('p_{T} [GeV]')

			if('MV2' in tagger):
				mg.GetHistogram().GetYaxis().SetRangeUser(-1.5,1.5)
			if('DL1' in tagger):
				mg.GetHistogram().GetYaxis().SetRangeUser(-2,7)

			mg.GetHistogram().GetXaxis().SetRangeUser(0,1000)
			#mg.Print("all");
			ROOT.gPad.Modified()
			ROOT.gPad.Update();


			leg.AddEntry(hybrid_cut_Profile,'hybrid cut','L')
			leg.AddEntry(hybrid_spline_cut,'hybrid spline','L')
			leg.AddEntry(flat_eff_cut_profile,'flat efficiency cut','L')
			leg.Draw('same')

			c.cd(2)
			if logXaxis:
				ROOT.gPad.SetLogx(1)

			hybrid_eff_Profile = infile.Get(tagger+'/'+jetCollection+'/OP'+op[1]+'/HybBEff/effvspthybridprofile')
			flat_cut_eff_profile = infile.Get(tagger+'/'+jetCollection+'/OP'+op[1]+'/FixedCut/efffromcutvspt'+op[1]+tagger)

			hybrid_eff_Profile.SetLineWidth(2)
			flat_cut_eff_profile.SetLineWidth(4)

			flat_cut_eff_profile.SetLineColor(ROOT.kRed)
			flat_cut_eff_profile.SetLineStyle(9)

			mg2 = ROOT.TMultiGraph()
			mg2.Add(hybrid_eff_Profile)
			mg2.Add(flat_cut_eff_profile)
			mg2.SetTitle(jetCollection+' '+tagger+' OP'+op[1]+' efficiency profiles')

			mg2.Draw('ALX')

			mg2.GetHistogram().GetYaxis().SetTitleOffset(1.4)
			mg2.GetHistogram().GetYaxis().SetTitle('b efficiency')
			mg2.GetHistogram().GetXaxis().SetTitle('p_{T} [GeV]')

			mg2.GetHistogram().GetYaxis().SetRangeUser(0.4,1)

			mg2.GetHistogram().GetXaxis().SetRangeUser(0,1000)

			#mg2.Print("all");

			ROOT.gPad.Modified()
			ROOT.gPad.Update();

			leg2 = ROOT.TLegend(0.5,0.8,0.9,0.9)
			leg2.AddEntry(hybrid_eff_Profile,'hybrid efficiency','L')
			leg2.AddEntry(flat_cut_eff_profile,'fixed cut efficiency','L')

			leg2.Draw('same')

			c.Print('HybridCuts/'+jetCollection+'_'+tagger+'_'+op[1]+'_profiles.png')
			#Y.T. I changed the folder. Originally ('Plots/HybridCuts/'+jetCollection+'_'+tagger+'_'+op[1]+'_profiles.png')
