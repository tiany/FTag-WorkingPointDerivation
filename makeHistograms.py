# How to run: python makeHistograms.py -c configFileName.json
import time
start_time=time.time()

# These imports are needed to load a json config file
# Json file: in an array, all array elements must have the same type.
from math import sqrt
import argparse,os,sys,json
import ROOT


parser=argparse.ArgumentParser(description='Make Plots')
parser.add_argument("-c", "--config", dest="c", type=str, required=True)
opt=parser.parse_args()

print 'Reading ".json" config file: ', opt.c
conf_file=open(opt.c, "r")
conf=json.load(conf_file)# conf is treated as a python dictionary.

cxxName='MakeHistograms.cxx'
ROOT.gROOT.ProcessLine('.L '+cxxName)
print "ROOT version:",ROOT.gROOT.GetVersion()


#------ ------ ------ ------ ------ ------
# A function that checks the variables specified in .json.
# If not specified and there is no default value, don't run the .cxx code,
# and tell the user.
# If there is a default value, use the default. And tell the user.
# If option == "str", change the corresponding dict entry to a string,
# if the variable is a list, change all its elements to string (only works
# for 1D lists).
def checkNone(dictName, keyName, option=None, defaultValue=None):
	if dictName.get(keyName)==None and defaultValue==None:
		print '!',keyName,'unspecified. Please specify it in',opt.c
		conf["runCxx"]=0
		return None
	elif dictName.get(keyName)==None and defaultValue!=None:
		if keyName!="flagLeafName2":# Because this variable is unnecessary
			print keyName,' unspecified in {}. Using default: {}'.format(opt.c,defaultValue)
		return defaultValue
	elif option=="str":
		theValue=dictName.get(keyName)
		if type(theValue)!=list:
			dictName[keyName]=str(theValue)
		else:
			for n in range(len(theValue)):
				theValue[n]=str(theValue[n])
			dictName[keyName]=theValue
		return dictName.get(keyName)# otherwise <type 'unicode'>
	else:
		return dictName.get(keyName)


# A function for the variables about the binning. 
# If the given is an int, make it a list. i.e. 2000 => [2000]
# If the given value is neither an int nor a list, use default value, 
# and tell the user.
# If there isn't a default value, don't run the .cxx. And tell the user.
def prepareList(dictName, keyName, defaultValue=None):
	theBins=dictName.get(keyName)
	if type(theBins)==int:
		conf[keyName]=[theBins]
	elif type(theBins)==list:
		pass
	elif defaultValue!=None:
		print '!',keyName,' in {} is neither an int nor a list. Using default: {}'.format(opt.c,defaultValue)
		conf[keyName]=defaultValue
	else:
		print '!',keyName,'should be either an int or a list. Please correct it in',opt.c
		conf["runCxx"]=0
	return conf[keyName]


#------ ------ ------ Prepare the variables ------ ------ ------

conf["runCxx"]=1# If =0, would't run the .cxx file.
inFileOrDir      =checkNone(conf,"inFileOrDir","str")
outFileName      =checkNone(conf,"outFileName","str")
jetCollection    =checkNone(conf,"jetCollection","str","AntiKt4EMPFlowJets_BTagging201903")
branchName       =checkNone(conf,"branchName","str")
jetParameterNames=checkNone(conf,"jetParameterNames","str")
flagLeafName1    =checkNone(conf,"flagLeafName1","str")
flagLeafName2    =checkNone(conf,"flagLeafName2","str","")
taggerLeafNames  =checkNone(conf,"taggerLeafNames")
# Change to string and extra check for taggerLeafNames. It checks that
# taggerLeafNames is a list, the elements are also lists, and the elements 
# have length 2. It doesn't check e.g. whether the names for the leaves 
# actually exist in the input root file, or whether the elements make sense.
if type(taggerLeafNames)==list:
	for i in range(len(taggerLeafNames)):#<type 'list'>
		if type(taggerLeafNames[i])==list:
			if len(taggerLeafNames[i])==2:
				for  j in range(len(taggerLeafNames[i])):
					taggerLeafNames[i][j]=str(taggerLeafNames[i][j])
			else: 
				print "! taggerLeafNames should be a list of lists. The elements in taggerLeafNames should have form: [taggerName, taggerOutputBranchName/Discriminant] (2 elements). The number of elements is wrong ({}). Please correct it in {}.".format(len(taggerLeafNames[i]),opt.c)
				conf["runCxx"]=0
				break
		else:
			print "! Your taggerLeafNames should be a list of lists. The elements in taggerLeafNames should have form: [taggerName, taggerOutputBranchName/Formula]. Now the type of this element is not list but: {}. Please correct it in {}.".format(type(taggerLeafNames[j]),opt.c)
			conf["runCxx"]=0
			break
elif type(taggerLeafNames)!=None:
	print "! Your taggerLeafNames should be a list of lists. The elements in taggerLeafNames should have form: [taggerName, taggerOutputBranchName/Formula]. Now the type of taggerLeafNames is not list but: {}. Please correct it in {}.".format(type(taggerLeafNames),opt.c)
	conf["runCxx"]=0

# Give default value to cutString.
cutString=str(conf.get("cutString"))
if cutString == "None" and len(jetParameterNames)==3:
	if jetCollection.find("PFlow")!=-1:
		cutString = "(!("+jetParameterNames[2]+"<=0.5&&abs("+jetParameterNames[1]+")<2.4&&"+jetParameterNames[0]+"<60000))&&"+jetParameterNames[0]+">20000&&abs("+jetParameterNames[1]+")<2.5"
		#cutString = "(!(jet_jvt<=0.5&&abs(jet_eta)<2.4&&jet_pt<60000))&&jet_pt>20000&&abs(jet_eta)<2.5"
		#print cutString
		print "cutString not specified! Using default value: ",cutString,"."
	elif jetCollection.find(EMTopo)!=-1:
		cutString = ""
		print "cutString not specified! No cuts on jet pT, eta,... are done."
elif cutString!="" and cutString!=" ":
	cutString=cutString+"&&"# "&&" is necessary because in the .cxx code it has to connect with the other requirements.
else:
	cutString = ""
	print "cutString not specified! No cuts on jet pT, eta,... are done."

# Check or give default value to the binning variables
ptBins=prepareList(conf,"ptBins",[10.0, 12.5, 15.0, 17.5, 20.0, 22.5, 25.0, 27.5, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100.0, 120.0, 140.0, 160.0, 180.0, 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 550.0, 600.0, 700.0, 800.0, 900.0, 1000.0, 1500.0, 2000.0, 3000.0])
etaBins=prepareList(conf,"etaBins",[25])
nTaggerDiscriminantBins=prepareList(conf,"nTaggerDiscriminantBins",[2000])

# You can print all the variables and have a look at them:
printAllVariables=0
if printAllVariables:
	nameList=[inFileOrDir,outFileName,jetCollection,branchName,jetParameterNames,flagLeafName1, flagLeafName2,taggerLeafNames,cutString,ptBins,etaBins,nTaggerDiscriminantBins]
	print "\nThe list of variables specified in {}: ".format(opt.c)
	for i in range(len(nameList)):
		print nameList[i]


if conf["runCxx"]==1:
	ROOT.MakeHistograms(inFileOrDir, outFileName, jetCollection, branchName, jetParameterNames, flagLeafName1, flagLeafName2, taggerLeafNames, cutString, ptBins, etaBins, nTaggerDiscriminantBins)
else:
	print "Didn't run {}. Please check the values you specified in your config file {}!".format(cxxName,opt.c)

print "# Runtime: --- {} seconds ---".format(time.time() - start_time)



