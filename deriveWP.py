# How to run: python deriveWP.py -c <configFileName.json>

import time
start_time = time.time()

# To load a json config file
from math import sqrt
import argparse,os,sys,json
import ROOT

parser =  argparse.ArgumentParser(description='Make Plots')
parser.add_argument("-c", "--config", dest="c", type=str, required=True)
opt = parser.parse_args()

print 'Reading json... File name: {}'.format(opt.c)
conf_file = open(opt.c, "r")
conf = json.load(conf_file)

# conf is treated as a python dictionary.
# print conf #Output: {u'taggers': [[u'DL1r', u'DL1r']], u'inputFileName': u'o2020-04-26/HistosJune2017v1.root', u'plotOutFileName': u'WP_Plots.root', u'operatingPoints': [[0.7, 70]], u'WP_File': u'WP_cuts_Oct2017.root', u'jetCollection': [u'AntiKt4EMPFlowJets']}. u means unicode. Thus type conversion to string is necessary.
cxxName='DeriveWP.cxx'
#ROOT.gROOT.ProcessLine('.L RejectionTool.cxx')
ROOT.gROOT.ProcessLine('.L WorkingPointTool.cxx')
ROOT.gROOT.ProcessLine('.L '+cxxName)
print "ROOT version:",ROOT.gROOT.GetVersion(),"\n"

#------ ------ ------ ------ ------ ------
# A function to conveniently save string in .txt file.
def saveInTxt(outTxtName, txtFileOption, theText):
	print theText
	theText=theText+"\n"
	fileout=open(outTxtName, txtFileOption)
	fileout.write(theText)
	fileout.close()
	return "a"# This is to judge whether to recreate a .txt file or simply update it next time. "a" is an option in python, for updating the file.  txtFileOption=saveInTxt(...)


# A function to check whether all needed variables are there.
# If a variable isn't specified and there isn't a default value for it, tell the user and don't run the .cxx file.
# If there is a default value, use the default value. Tell the user unless it's "printout" (unnecessary).
# If option is "str", convert to string.
# If option contains "list": 1. check that it's a list. 2. if it's a list of lists, make the 1st (0th) element of the element string. 3. if "str" in option, make the whole element string.
def checkNone(dictName, keyName, option=None, defaultValue=None):
	if dictName.get(keyName)==None and defaultValue==None:
		print '! "{}" unspecified. Please specify it in {}.'.format(keyName, opt.c)
		conf["runCxx"]=0
		return None
	elif dictName.get(keyName)==None and defaultValue!=None:
		if keyName!="printout":
			print '"{}" unspecified in {}. Using default: {}'.format(keyName, opt.c, defaultValue)
		return defaultValue
	elif option=="str":
		return str(dictName.get(keyName))# otherwise <type 'unicode'>
	elif "list" in option:
		if type(dictName.get(keyName))!=list:
			print '! "{}" should be a list, but now it is {}. Please check your {}.'.format(keyName,type(dictName.get(keyName)),opt.c)
			conf["runCxx"]=0
			return dictName.get(keyName)
		else:
			for i in range(len(dictName.get(keyName))):
				if type(dictName.get(keyName)[i])==list: #If list within list, make 1st element str
					dictName.get(keyName)[i][0]=str(dictName.get(keyName)[i][0])
				else:
					if "str" in option:
						dictName.get(keyName)[i]=str(dictName.get(keyName)[i])
					else:
						pass
			return dictName.get(keyName)
	else:
		return dictName.get(keyName)


#------ ------ ------ Prepare the variables ------ ------ ------

conf["runCxx"]=1 # If =0, don't run the .cxx file.

inputFileName=checkNone(conf, "inputFileName", "str")
# Make sure input file name ends with ".root". If no .root, can't be opened.
if inputFileName!=None and inputFileName.endswith(".root"):
	#print "Good, specified input file name contains \".root\"."
	print "Input file name:   ",inputFileName
elif inputFileName!=None:
	print "Input file name does not contain \".root\". Appending one."
	inputFileName=inputFileName+".root"
	print "Input file name:   ",inputFileName

plotOutFileName=checkNone(conf, "plotOutFileName", "str")
# Make sure plotOutFileName ends with ".root".
if plotOutFileName!=None and plotOutFileName.endswith(".root"):
	#print "Good, specified name for output plot file contains \".root\"."
	print "plotOutFile name:  ",plotOutFileName
elif plotOutFileName!=None:
	print "Specified name for output plot file does not contain \".root\". Appending one."
	plotOutFileName=plotOutFileName+".root"
	print "plotOutFile name:  ",plotOutFileName

customCDIfile=checkNone(conf,"customCDIfile","str")
# Check that customCDIfile name ends with ".root" and create a name for the .txt file that saves the values. 
outTxtName=""
if customCDIfile!=None and customCDIfile.endswith(".root"):
	#print "Good, specified name for custom CDI file contains \".root\"."
	print "customCDIfile name:",customCDIfile
	for i in range(len(customCDIfile)-5):
		outTxtName=outTxtName+customCDIfile[i]
	outTxtName=outTxtName+".txt"
	print "outTxtName:        ",outTxtName
elif customCDIfile!=None:
	print "Specified name for custom CDI file does not contain \".root\". Appending one."
	outTxtName=customCDIfile+".txt"
	customCDIfile=customCDIfile+".root"
	print "customCDIfile name:",customCDIfile
	print "outTxtName:        ",outTxtName

# Separate the taggers to a list of tagger names and a list of fc values. Because c++ doesn't allow different types in the same vector, unless I make a class for it. 
taggers=checkNone(conf,"taggers","list-str")
print "taggers selected:       ",taggers
taggerNames=[]
fcs=[]
for n in range(len(taggers)):
	if type(taggers[n])!=list:# MV2 series tagger doesn't have an fc. Append -1 (same as in central CDI).
		taggerNames.append(str(taggers[n]))
		fcs.append(-1)
	else:
		taggerNames.append(str(taggers[n][0]))
		fcs.append(taggers[n][1])

specified=conf.get("jetCollection")
jetCollection=checkNone(conf,"jetCollection","str","AntiKt4EMPFlowJets_BTagging201903")
if specified!=None:# Only print if it wasn specified. If used default, would have printed in the function "checkNone"
	print "jetCollection:	        ",jetCollection

# Change to 2 lists. Similar to that for taggers
OPlist               =checkNone(conf,"operatingPoints","list")
print "Working points selected:",OPlist
# The specified WP are e.g. 70. Convert to a float like 0.7
# Elements in the newly made list operatingPoints: e.g. [0.7, '70'] if it's a WP. ['cRej 50', 50] if it's a rejection.
operatingPointNames=[]
operatingPoints=[]
flatRejection=False
for n in range(len(OPlist)):
	if type(OPlist[n])!=list:
		operatingPointNames.append(str(OPlist[n]))#e.g. [0.7, '70']
		operatingPoints.append(OPlist[n]/100.)
	else:
		operatingPointNames.append(OPlist[n][0]+str(OPlist[n][1]))
		operatingPoints.append(OPlist[n][1])
		flatRejection=True

WP_profiles          =checkNone(conf,"WP_profiles","list-str")
if flatRejection==True:
	WP_profiles.append("flatRejection")
print "WP profiles selected:   ",WP_profiles
WP_profiles          =str(WP_profiles)
#print WP_profiles

# If user doesn't want hybrid, make minPtForHybridSpline=-9, regardless of what was specified.
if ("hybrid" in WP_profiles):
	specified=conf.get("minPtForHybridSpline")
	minPtForHybridSpline =checkNone(conf,"minPtForHybridSpline","",25)# Slightly inefficient
	if specified!=None:
		print "minPtForHybridSpline:   ",minPtForHybridSpline
else:
	minPtForHybridSpline=-9

# The variables that are modified and have forms different from in .json:
#print "working points:", operatingPointNames# Output: [70, ['uRej', 502]]
#print "working points:", operatingPoints# Output: [0.7, 502]
#print "tagger names:", taggerNames#Output: ['MV2c10', 'DL1r']
#print "fcs:",fcs#Output: [-1, 0.018]
printout=checkNone(conf,"printout","",True)

if conf["runCxx"]==1:
	print "\n"
	ROOT.deriveWorkingPoints(inputFileName, plotOutFileName, customCDIfile, outTxtName, taggerNames, fcs, jetCollection, operatingPointNames, operatingPoints, WP_profiles, minPtForHybridSpline, printout)# outTxtName has to be already created to save the time here. But potentially could move the modification of variables to .cxx
	txtFileOption=saveInTxt(outTxtName,"a","# Runtime: --- {} seconds ---".format(time.time() - start_time))# txt file option "w" for the 1st time, "a" for update
else:
	print "Didn't run {}. Please check the values you specified in your config file {}!".format(cxxName,opt.c)



